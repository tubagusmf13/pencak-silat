import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DataPageComponent } from './component/page-tekmit/data-page/data-page.component';
import { DetailCompetitionPageComponent } from './component/page-tekmit/detail-competition-page/detail-competition-page.component';
import { ReportTekmitComponent } from './component/page-tekmit/report-tekmit/report-tekmit.component';
import { CompetitionSchemePageComponent } from './component/page-tekmit/competition-scheme-page/competition-scheme-page.component';
import { MonitoringCompetitionComponent } from './component/page-match/monitoring-competition/monitoring-competition.component';
import { ReportMatchComponent } from './component/page-match/report-match/report-match.component';
import { ViewPenontonComponent } from './component/page-match/view-penonton/view-penonton.component';
import { ViewDewanJuriComponent } from './component/page-match/view-dewan-juri/view-dewan-juri.component';
import { HomePageComponent } from './component/page-online/home-page/home-page.component';
import { NavbarOnlineComponent } from './component/page-online/navbar-online/navbar-online.component';
import { FooterOnlineComponent } from './component/page-online/footer-online/footer-online.component';
import { EventChampionshipComponent } from './component/page-online/event-championship/event-championship.component';
import { RegisterSelectChampionshipComponent } from './component/page-online/register-select-championship/register-select-championship.component';
import { RegisterContingentComponent } from './component/page-online/register-contingent/register-contingent.component';
import { RegisterParticipantComponent } from './component/page-online/register-participant/register-participant.component';
import { PageLoginComponent } from './component/page-online/page-login/page-login.component';
import { DataChampionshipComponent } from './component/page-tekmit/data-page/data-championship/data-championship.component';
import { DataContingentComponent } from './component/page-tekmit/data-page/data-contingent/data-contingent.component';
import { DataJurorComponent } from './component/page-tekmit/data-page/data-juror/data-juror.component';
import { DataParticipantComponent } from './component/page-tekmit/data-page/data-participant/data-participant.component';
import { ReportClassChampionComponent } from './component/page-match/report-match/report-class-champion/report-class-champion.component';
import { ReportComptScheduleComponent } from './component/page-match/report-match/report-compt-schedule/report-compt-schedule.component';
import { ReportComptSchemeComponent } from './component/page-match/report-match/report-compt-scheme/report-compt-scheme.component';
import { ReportMedalListComponent } from './component/page-match/report-match/report-medal-list/report-medal-list.component';
import { ReportWeightValidationComponent } from './component/page-match/report-match/report-weight-validation/report-weight-validation.component';
import { ReportCompetitionSchemeComponent } from './component/page-tekmit/report-tekmit/report-competition-scheme/report-competition-scheme.component';
import { ReportListParticipantComponent } from './component/page-tekmit/report-tekmit/report-list-participant/report-list-participant.component';
import { ViewStatisticsComponent } from './component/page-match/view-statistics/view-statistics.component';

import { AuthGuard } from './services/auth-guard.service';
import { SignInComponent } from './component/sign-in/sign-in.component';


const routes: Routes =
  [
    { path : '', redirectTo: 'signin', pathMatch: 'full'},
    { path: 'login',  component: PageLoginComponent },
    { path: 'home',  component: HomePageComponent },
    { path: 'championship',  component: RegisterSelectChampionshipComponent, canActivate: [AuthGuard]},
    { path: 'contingent',  component: RegisterContingentComponent, canActivate: [AuthGuard]},
    { path: 'participant',  component: RegisterParticipantComponent, canActivate: [AuthGuard]},
    { path: 'event',  component: EventChampionshipComponent },

    //OPERATOR    
    { path: 'signin', component: SignInComponent},
    ///--------------------------////
    { path: 'data', component: DataPageComponent, canActivate: [AuthGuard]},
    { path: 'competition-scheme', component: CompetitionSchemePageComponent, canActivate: [AuthGuard]},
    { path: 'detail-competition', component: DetailCompetitionPageComponent, canActivate: [AuthGuard]},
    { path: 'report', component: ReportTekmitComponent, canActivate: [AuthGuard]},
    { path: 'monitoring-competition', component: MonitoringCompetitionComponent, canActivate: [AuthGuard]},
    { path: 'report-match', component: ReportMatchComponent, canActivate: [AuthGuard]},
    { path: 'view-penonton', component: ViewPenontonComponent, canActivate: [AuthGuard]},
    { path: 'view-dewan-juri', component: ViewDewanJuriComponent, canActivate: [AuthGuard]},
    { path: 'data-championship',  component: DataChampionshipComponent , canActivate: [AuthGuard]}, 
    { path: 'data-contingent',  component: DataContingentComponent , canActivate: [AuthGuard]},
    { path: 'data-participant',  component: DataParticipantComponent , canActivate: [AuthGuard]},
    { path: 'data-juror',  component: DataJurorComponent , canActivate: [AuthGuard]},
    { path: 'report-list-participant', component: ReportListParticipantComponent, canActivate: [AuthGuard]},
    { path: 'report-competition-scheme', component: ReportCompetitionSchemeComponent, canActivate: [AuthGuard]},
    { path: 'report-compt-scheme', component: ReportComptSchemeComponent, canActivate: [AuthGuard]},
    { path: 'report-compt-schedule', component: ReportComptScheduleComponent, canActivate: [AuthGuard]},
    { path: 'report-class-champion', component: ReportClassChampionComponent, canActivate: [AuthGuard]},
    { path: 'report-weight-validation', component: ReportWeightValidationComponent, canActivate: [AuthGuard]},
    { path: 'report-medal-list', component: ReportMedalListComponent, canActivate: [AuthGuard]},
    { path: 'view-statistics', component: ViewStatisticsComponent, canActivate: [AuthGuard]},
    
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{ useHash: true })],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
