import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';

import { Competition } from '../model/competition'
import { Scheme } from "../model/scheme";
import { Monitoring } from '../model/monitoring';
import { Score } from "../model/score";
import { Arena } from "../model/arena";
import { Schedule } from "../model/schedule";


@Injectable()
export class CompetitionService {

  private generateUrl = "http://localhost:8000/competition/generate";
  private getUrl = "http://localhost:8000/api/competition/scheme/";
  private getUrlschemeAll = "http://localhost:8000/api/competition/schemeAll/All";
  private getUrlMonitor = "http://localhost:8000/api/competition/show/monitoring/";
  private postScoreUrl = "http://localhost:8000/api/monitoringCompetition";
  private getListMonitoring = "http://localhost:8000/api/competition/show/monitoring/list/?arena=";
  private getScoringUrl = "http://localhost:8000/api/showScore/?competition=";
  private getRoundClassUrl = "http://localhost:8000/api/competition/show/round/list/?class=";
  private getArenaUrl = "http://localhost:8000/api/arena";
  private getScheduleList = "http://localhost:8000/api/competition/show/schedule/";
  private getUrlMonitorByDate = "http://localhost:8000/api/competition/show/monitoringByDate/"; // @kaha
  private getScheduleSession = "http://localhost:8000/api/competition/show/scheduleSession/list/?arena="; // @kaha


  constructor(private http : Http) { }

  getSchedule(): Observable<Schedule[]> {
    return this.http.get(this.getScheduleList + "?championship=" + localStorage.getItem("getChampId"))
                    .map(res => { return <Schedule[]>res.json(); } )
                    .catch(this.handleError);
  }

  getRoundClass(classId): Observable<Score[]> {
    return this.http.get(this.getRoundClassUrl + classId)
                .map(res => {
                  return <Score[]>res.json();})
                .catch(this.handleError);
  }

  getArena(): Observable<Arena[]> {
    return this.http.get(this.getArenaUrl)
                    .map(res => { return <Arena[]>res.json(); } )
                    .catch(this.handleError);
  }

  postComptGenerate(body : Competition){
    let bodyStringJSON = JSON.stringify(body);
     
   return this.http.post(this.generateUrl, bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  getSchemeId(schemeId): Observable<Competition[]> {
    return this.http.get(this.getUrl + schemeId )
                    .map(res => { return <Competition[]>res.json(); } )
                    .catch(this.handleError);
  }

  getScore(numCompt): Observable<Score[]> {
    return this.http.get(this.getScoringUrl + numCompt )
                    .map(res => { return <Score[]>res.json(); } )
                    .catch(this.handleError);
  }

  getSchemeAll(): Observable<Scheme[]> {
    return this.http.get(this.getUrlschemeAll)
                    .map(res => { return <Scheme[]>res.json(); } )
                    .catch(this.handleError);
  }

  getMonitoring(url : String,arena){
    return this.http.get(this.getUrlMonitor + url + "?arena=" + arena + "&championship=" + localStorage.getItem("getChampId")) 
    .map(res => {
      return <Monitoring[]>res.json();})
    .catch(this.handleError);
  }

  getMonitoringList(arena){
    return this.http.get(this.getListMonitoring + arena + "&championship=" + localStorage.getItem("getChampId"))
    .map(res => {
      return <Monitoring[]>res.json();})
    .catch(this.handleError);
  }
  // begin @kaha
  getScheduleSessionList(dataTgl,arena){
    return this.http.get(this.getScheduleSession + arena + "&championship=" + localStorage.getItem("getChampId")+ "&date=" + dataTgl)
    .map(res => {
      return <Monitoring[]>res.json();})
    .catch(this.handleError);
  }
  getMonitoringByDate(url : String,arena){
    return this.http.get(this.getUrlMonitorByDate + url + "&arena=" + arena + "&championship=" + localStorage.getItem("getChampId")) 
    .map(res => {
      return <Monitoring[]>res.json();})
    .catch(this.handleError);
  }
  //end @kaha

  postScoring(body : Monitoring){
    let bodyStringJSON = JSON.stringify(body);
    return this.http.post(this.postScoreUrl, bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  } 
    

}
