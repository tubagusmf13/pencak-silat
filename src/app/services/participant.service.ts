import { Injectable } from "@angular/core";
import {
  Headers,
  Http,
  Response,
  RequestOptions,
  RequestMethod
} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/publishReplay";
import "rxjs/add/operator/do";
import { Participant } from "../model/participant";

@Injectable()
export class ParticipantService {
  private participantUrl = "http://localhost:8000/api/participant/?championship=" + localStorage.getItem("getChampId");
  private saveDelUrl = "http://localhost:8000/api/participant/" ;

  //private participantUrl = "http://192.168.100.33:8000/api/participant";
  res: Response;

  constructor(private http: Http) {}

  getParticipant(): Observable<Participant[]> {
    return this.http
      .get(this.participantUrl)
      .map(res => {
        return <Participant[]>res.json();
      })
      .catch(this.handleError);
  }

  getParticipantFilter(name,cont,kelas,gender) {
    return this.http
      .get(this.participantUrl + "&name="+ name+ "&contingent="+ cont+ "&className="+ kelas+"&gender="+gender)
      .map(res => {
        return <Participant[]>res.json();
      })
      .catch(this.handleError);
  }

  getParticipantClassGender(kelas,gender): Observable<Participant[]> {
    return this.http
      .get(this.participantUrl + "?class="+ kelas+"&gender="+gender)
      .map(res => {
        return <Participant[]>res.json();
      })
      .catch(this.handleError);
  }

  postParticipant(body: Participant[]) {
    let bodyStringJSON = JSON.stringify(body);
    return this.http
      .post(this.saveDelUrl, bodyStringJSON, null)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json || "Server Error"));
  }

  addParticipant(body : Participant){
    let bodyStringJSON = JSON.stringify(body);
      
     
   return this.http.post(this.saveDelUrl + "save", bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  } 

  deleteParticipant(partId: Participant): Observable<number> {
    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .get(this.saveDelUrl + "del/" + partId)
      .map(success => success.status)
      .catch(this.handleError);
  }

  editParticipant(partId ,  body: Participant) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let bodyStringJSON = JSON.stringify(body);
    
    return this.http
      .post(this.saveDelUrl + "put/" + partId, bodyStringJSON, null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  getParticipantId(partId: Participant): Observable<Participant> {
    return this.http.get(this.saveDelUrl + "id/" + partId).map(res => {
      return <Participant>res.json();
    }).catch(this.handleError);
  }

  private extractData(res: Response){ 
    let body = res.json(); 
    return body.data; 
  } 

  private handleError(res) {
    return Observable.throw(res.statusText);
  }
}
