import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { User } from '../model/user';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';

@Injectable()
export class LoginService {

  constructor(private http:Http) { }
  private LoginUserUrl = "http://localhost:8000/login/";
  //private LoginUserUrl = "http://192.168.100.33:8000/login/?username=";

  SignIn(user : User){
   return this.http.get(this.LoginUserUrl+user.username+"/"+user.password)
      .map((res:Response) => res.json())
      .catch((res:Response) => Observable.throw(res.json() )); 
  }

}
