import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';
import { Juror } from '../model/juror'
import { Schedule } from '../model/schedule'


@Injectable()
export class JurorService {

  private addJurorUrl = "http://localhost:8000/api/juror/";
  private addGrandJurorUrl = "http://localhost:8000/api/grandJuror/"; 
  private showJurorIdUrl = "http://localhost:8000/showJurorGrand/"; 
  private editJurorIdUrl = "http://localhost:8000/editJurorGrand/"; 

  private addAllUrl = "http://localhost:8000/allJurorGrand/?championship=" + localStorage.getItem("getChampId");
  private jurorUrl = "http://localhost:8000/api/juror/?championshipId=" + localStorage.getItem("getChampId");
  private grandJurorUrl = "http://localhost:8000/api/grandJuror/?championshipId=" + localStorage.getItem("getChampId");

  //private chamionUrl = "http://192.168.100.33:8000/api/championship";

  
  constructor(private http: Http) {  }

  postGrandJuror(body : Juror){
    let bodyStringJSON = JSON.stringify(body);     
   return this.http.post(this.addGrandJurorUrl, bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  } 

  postJuror(body : Juror){
    let bodyStringJSON = JSON.stringify(body);
   return this.http.post(this.addJurorUrl, bodyStringJSON,null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  } 

  getJurors(): Observable<Schedule[]> {
    return this.http.get(this.jurorUrl)
                    .map(res => {
                      return <Schedule[]>res.json();})
                    .catch(this.handleError);
  }

  getJurorsFilter(juror,role,gender): Observable<Juror[]> {
    return this.http.get(this.showJurorIdUrl + '?name=' + juror + '&role=' + role + '&gender=' + gender )
                    .map(res => {
                      return <Juror[]>res.json();})
                    .catch(this.handleError);
  }

  getJuror(): Observable<Juror[]> {
    return this.http.get(this.jurorUrl)
                    .map(res => {
                      return <Juror[]>res.json();})
                    .catch(this.handleError);
  }

  getAll(): Observable<Juror[]> {
    return this.http.get(this.addAllUrl)
                    .map(res => {
                      return <Juror[]>res.json();})
                    .catch(this.handleError);
  }

  getGrandJuror(): Observable<Schedule[]> {
    return this.http.get(this.grandJurorUrl)
                    .map(res => {
                      return <Schedule[]>res.json();})
                    .catch(this.handleError);
  }

  getJurorId(jurId,role: string): Observable<Juror> {
    return this.http.get(this.showJurorIdUrl+"?jurorId="+jurId + "&role=" + role )
                    .map(res => {
                      return <Juror>res.json();})
                    .catch(this.handleError);
  }

  deleteJuror(jurId: Juror): Observable<number> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.get(this.addJurorUrl +"del/"+ jurId)
           .map(success => success.status)
                 .catch(this.handleError);        
      }	

  editJuror(body: Juror ) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let bodyStringJSON = JSON.stringify(body);
    
    return this.http
      .post(this.editJurorIdUrl, bodyStringJSON, null)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json || 'Server Error' ));
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  } 

}
