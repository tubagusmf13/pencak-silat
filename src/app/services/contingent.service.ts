import { Injectable } from "@angular/core";
import {
  Headers,
  Http,
  Response,
  RequestOptions,
  RequestMethod
} from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/operator/publishReplay";
import "rxjs/add/operator/do";
import { Contingent } from "../model/contingent";

@Injectable()
export class ContingentService {
  private contingentUrl = "http://localhost:8000/api/contingent/";
  private contingentIdUrl = "http://localhost:8000/api/contingent/id/";

  // private contingentUrl = "https://192.168.100.33:8000/api/contingent";

  constructor(private http: Http) {}

  getContingent(): Observable<Contingent[]> {
    return this.http
      .get(
        this.contingentUrl +
          "?championship=" +
          localStorage.getItem("getChampId")
      )
      .map(res => {
        return <Contingent[]>res.json();
      })
      .catch(this.handleError);
  }

  getContingentFilter(cont, official): Observable<Contingent[]> {
    return this.http
      .get(
        this.contingentUrl +
          "?championship=" + localStorage.getItem("getChampId") + "&name=" +
          cont +
          "&official=" +
          official
      )
      .map(res => {
        return <Contingent[]>res.json();
      })
      .catch(this.handleError);
  }

  postContingent(body: Contingent) {
    let bodyStringJSON = JSON.stringify(body);

    return this.http
      .post(this.contingentUrl + "official" , bodyStringJSON, null)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json || "Server Error"));
  }

  deleteContingent(contId: Contingent): Observable<number> {
    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .get(this.contingentUrl + "del/" + contId)
      .map(success => success.status)
      .catch(this.handleError);
  }

  getContingentId(contId: Contingent): Observable<Contingent> {
    return this.http
      .get(this.contingentIdUrl + contId  + "/"+ localStorage.getItem("getChampId"))
      .map(res => {
        return <Contingent>res.json();
      })
      .catch(this.handleError);
  }

  editContingent(contId, body: Contingent) {
    let bodyStringJSON = JSON.stringify(body);

    return this.http
      .post(this.contingentUrl + "put/" + contId, bodyStringJSON, null)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json || "Server Error"));
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  }
}
