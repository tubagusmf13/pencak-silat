import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions ,RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/do';

import { Schedule } from '../model/schedule'


@Injectable()
export class ScheduleService {

  private url = "http://localhost:8000/api/competition/?championship=" + localStorage.getItem("getChampId");
  private post = "http://localhost:8000/api/competition/setSchedule";
  private getUrl = "http://localhost:8000/api/competition/show/scheduled/?date=";
  private getfilter = "http://localhost:8000/api/competition/show/schedule/?date=";
  private deleteSchedUrl = "http://localhost:8000/api/competition/delete/schedule";
  private getListUrl = "http://localhost:8000/api/competition/show/scheduled/list/?date=";


  constructor(private http: Http) {  }

  getScheduleScheme(kelas, round): Observable<Schedule[]> {
    return this.http.get(this.url + "&qualification=" + round + "&class=" +kelas)
                .map(res => {
                  return <Schedule[]>res.json();})
                .catch(this.handleError);
  }

  postSchedules(body: Schedule[]) {
    let bodyStringJSON = JSON.stringify(body);
    return this.http
      .post(this.post, bodyStringJSON, null)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json || "Server Error"));
  }

  deleteSchedule(body: Schedule) {
    let bodyStringJSON = JSON.stringify(body);
    return this.http
      .post(this.deleteSchedUrl, bodyStringJSON, null)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json || "Server Error"));
  }

  getSession(arena, date){
    return this.http.get(this.getListUrl + date + "&arena=" + arena + "&championship=" + localStorage.getItem("getChampId") )
    .map(res => {
      return <Schedule[]>res.json();})
    .catch(this.handleError);
  }

  getScheduleFilter(arena, date, time){
    return this.http.get(this.getfilter + date + "&arena=" + arena + "&championship=" + localStorage.getItem("getChampId") + time)
    .map(res => {
      return <Schedule[]>res.json();})
    .catch(this.handleError);
  }
  
  getSchedules(date,start,end,arena){
    return this.http.get(this.getUrl + date + "&timeStart=" + start + "&timeEnd=" + end + "&arena=" + arena + "&championship=" + localStorage.getItem("getChampId") )
    .map(res => {
      return <Schedule[]>res.json();})
    .catch(this.handleError);
  }

  private handleError(res) {
    return Observable.throw(res.statusText);
  } 

}
