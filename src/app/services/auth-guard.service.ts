import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate() {
    if (localStorage.getItem('operator')) {
      return true;
    } else if (localStorage.getItem('official')){
      return true;
    }

  //this.router.navigate(['/login']);
  alert("Silahkan Login Dulu");
  return false;
  }

}
