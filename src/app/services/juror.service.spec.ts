import { TestBed, inject } from '@angular/core/testing';
import { JurorService } from './juror.service';

describe('JurorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JurorService]
    });
  });

  it('should ...', inject([JurorService], (service: JurorService) => {
    expect(service).toBeTruthy();
  }));
});
