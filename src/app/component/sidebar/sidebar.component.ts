import { Component, Renderer2, OnInit, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private _document) { }

  public baseUrl = this._document.location.origin;

  ngOnInit() {
    
  }

  onViewDewan(url){
    //this.router.navigateByUrl("https://www.google.com");
    var win = window.open(this.baseUrl+url, '_blank' , 'width=1000; zoom=70%;');
    win.focus();
    
  } 
  

  onViewPenonton(url){
    //this.router.navigateByUrl("https://www.google.com");
    var win = window.open(this.baseUrl+url, '_blank' , 'width=1000; zoom=80%;');
    win.focus();
    } 
}
