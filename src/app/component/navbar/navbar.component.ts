import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ChamphionshipService } from "../../services/champhionship.service";

import { Championship } from "../../model/championship";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"],
  providers: [ChamphionshipService]
})
export class NavbarComponent implements OnInit {
  private champ: Championship[];
  private champList = [];
  public champIdSelect;
  public champName;
  public bulan = [
    "_",
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];

  constructor(
    public router: Router,
    public champService: ChamphionshipService
  ) {}

  ngOnInit() {
    this.getChampion();
    this.champIdSelect = localStorage.getItem("getChampId");
    this.champName = localStorage.getItem("getChampName");
  }

  getChampion() {
    this.champService.getChampionship().subscribe(
      res => {
        this.champ = res;
        for (var i = 0; i < res.length; i++) {
          this.champList[res[i].championshipId] = res[i];
        }
      },
      er => console.log(er)
    );

    //this.champName = this.champ;
  }

  onChamp(event) {
    localStorage.removeItem("getChampId");
    let champId = event.target.value;

    let startDate = new Date(this.champList[champId].dateStart);
    let endDate = new Date(this.champList[champId].dateEnd);

    let awal =
      startDate.getDate().toString() +
      " " +
      this.bulan[startDate.getMonth()] +
      " " +
      startDate.getFullYear().toString();
    let akhir =
      endDate.getDate().toString() +
      " " +
      this.bulan[endDate.getMonth()] +
      " " +
      endDate.getFullYear().toString();

    this.champName = this.champList[champId].championshipName;
    let champTgl = awal + " - " + akhir;


    localStorage.setItem("getChampId", champId);
    localStorage.setItem("getChampName", this.champName);
    localStorage.setItem("getChampTgl", champTgl);
    localStorage.setItem("getChampDesc", this.champList[champId].description);
    localStorage.setItem("getChampLocation", this.champList[champId].location);
    localStorage.setItem("getChampDateStart", this.champList[champId].dateStart);
    localStorage.setItem("getChampDateEnd", this.champList[champId].dateEnd);

    this.champIdSelect = localStorage.getItem("getChampId");
  }
}
