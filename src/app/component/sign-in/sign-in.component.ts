import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { LoginService } from "../../services/login.service";

import { User } from "../../model/user";
import { Router } from "@angular/router";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.css"],
  providers: [LoginService]
})
export class SignInComponent implements OnInit {
  private newUser: any;
  public myLoginForm: FormGroup;
  public isSumbitted: boolean;
  //public loading:true;
  constructor(
    private _formBuilder: FormBuilder,
    private lService: LoginService,
    private router: Router
  ) {}
  ngOnInit() {

    localStorage.removeItem('operator');
    this.myLoginForm = this._formBuilder.group({
      username: ["", <any>Validators.required],
      password: ["", [<any>Validators.required]]
    });
    this.isSumbitted = false;
  
  }

  onSubmit(event, user: User, isValid: boolean): void {
    //this.loading = true;
    event.preventDefault();

    this.isSumbitted = true;
    if (isValid) 
    {
      this.lService.SignIn(user).subscribe(res => {
        if (res.loginAs == "operator") 
        {
          if (res.status == "success") 
          {
            alert("Login Success !");
            localStorage.setItem("operator", JSON.stringify(res));
            this.router.navigate(["/data"]);
          } else if (res.status == "fail") 
          {
            alert("Login Failed !");
          }
        } else if (res.loginAs != "operator") {
          alert("username / password incorrect");
        } else if (res.status == "fail") {
          alert("Login Failed !");
        }
      });
    }
    //this.isSumbitted=false;
  }
}
