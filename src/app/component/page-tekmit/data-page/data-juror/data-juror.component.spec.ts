import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataJurorComponent } from './data-juror.component';

describe('DataJurorComponent', () => {
  let component: DataJurorComponent;
  let fixture: ComponentFixture<DataJurorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataJurorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataJurorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
