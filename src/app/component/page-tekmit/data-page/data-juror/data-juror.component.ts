import { Component, OnInit, ViewChild } from "@angular/core";
import { JurorService } from "../../../../services/juror.service";
import { Router } from "@angular/router";
//import { Popup } from 'ng2-opd-popup';
import { Juror } from "../../../../model/juror";
import { Observable } from "rxjs/Observable";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

@Component({
  selector: "app-data-juror",
  templateUrl: "./data-juror.component.html",
  styleUrls: ["./data-juror.component.css"],
  providers: [JurorService]
})
export class DataJurorComponent implements OnInit {
  private jurors: Juror[];
  private jurorsId: Juror;
  private id;
  public formEdit: FormGroup;
  public formAdd: FormGroup;

  public formAdd2: FormGroup;

  statusCode: number;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private jurorService: JurorService
  ) {}

  ngOnInit() {
    this.getJurors();

    this.formEdit = this._formBuilder.group({
      jurorName: "",
      gender: "",
      role: ""
    });
    this.formAdd = this._formBuilder.group({
      jurorName: "",
      gender: "",
      role: "JUROR"
    });
    this.formAdd2 = this._formBuilder.group({
      grandJurorName: "",
      gender: "",
      role: "GRAND JUROR"
    });
  }

  onSubmit(juror: Juror): void {
    if (juror.jurorName != "" && juror.gender != "") {
      this.id = localStorage.getItem("getChampId");
      juror.championshipId = this.id;
      this.jurorService.postJuror(juror).subscribe(
        res => {
          console.log(res);
          if (res.status == "success") {
            alert("Add Succes !");
          } else {
            alert(res.message);
          }
        },
        err => console.log(err)
      );
      this.getJurors();
    } else {
      alert("Form must be filled!");
    }
  }

  createItem(jur: Juror): FormGroup {
    return this._formBuilder.group({
      jurorName: [jur.jurorName],
      role: [jur.role],
      gender: [jur.gender]
    });
  }

  onSubmit2(grandJur: Juror): void {
    if (grandJur.grandJurorName != "" && grandJur.gender != "") {
      this.id = localStorage.getItem("getChampId");
      grandJur.championshipId = this.id;
      this.jurorService.postGrandJuror(grandJur).subscribe(
        res => {
          if (res.status == "success") {
            alert("Add Succes !");
          } else {
            alert(res.message);
          }
        },
        err => console.log(err)
      );
      this.getJurors();
    } else {
      alert("Form must be filled!");
    }
  }

  getJurors(): void {
    this.jurorService.getAll().subscribe(
      champ => {
        this.jurors = champ;
      },
      er => console.log(er)
    );
  }

  onFilter(juror, role, gender): void {
    this.jurorService.getJurorsFilter(juror, role, gender).subscribe(
      cat => {
        this.jurors = cat;
      },
      er => console.log(er)
    );
  }

  deleteJurors(jurId: Juror) {
    this.jurorService.deleteJuror(jurId).subscribe(
      cat => {
        this.statusCode = 204;
        alert("Delete Juror Success");
      },

      er => console.log(er)
    );
    this.getJurors();
  }

  // @ViewChild('popup1') popup1: Popup;

  updateJurors(jurId: any, role: string) {
    this.id = jurId;
    this.jurorService.getJurorId(jurId, role).subscribe(
      part => {
        this.jurorsId = part;
        this.formEdit = this.createItem(this.jurorsId);
      },
      er => console.log(er)
    );
  }

  Update(jurorsUpdate: Juror) {
    jurorsUpdate.jurorId = this.id;
    this.jurorService.editJuror(jurorsUpdate).subscribe(
      res => {
        if (res.status == "success") {
          alert("Update Success !");
        } else {
          alert(res.message);
        }
      },

      err => console.log(err)
    );
    this.getJurors();
  }
}
