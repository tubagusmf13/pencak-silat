import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataContingentComponent } from './data-contingent.component';

describe('DataContingentComponent', () => {
  let component: DataContingentComponent;
  let fixture: ComponentFixture<DataContingentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataContingentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataContingentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
