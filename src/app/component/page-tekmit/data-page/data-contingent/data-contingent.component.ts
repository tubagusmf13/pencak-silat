import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { ParticipantService } from "../../../../services/participant.service";
import { ContingentService } from "../../../../services/contingent.service";
import { Router } from "@angular/router";
//import { Popup } from 'ng2-opd-popup';

import { Participant } from "../../../../model/participant";
import { Contingent } from "../../../../model/contingent";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-data-contingent",
  templateUrl: "./data-contingent.component.html",
  styleUrls: ["./data-contingent.component.css"],
  providers: [ContingentService]
})
export class DataContingentComponent implements OnInit {
  private contingetnsId: Contingent;
  private contingents: Contingent[];
  private errorMessage: string;
  public formAdd: FormGroup;
  public formEdit: FormGroup;
  private id;

  statusCode: number;

  constructor(
    private _formBuilder: FormBuilder,
    private contService: ContingentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getContingents();
    this.postContingents();

    this.formEdit = this._formBuilder.group({
      contingentName: "",
      contingentAddress: "",
      officialName: ""
    });
  }

  onFilter(cont, official): void {
    this.contService.getContingentFilter(cont, official).subscribe(
      cat => {
        this.contingents = cat;
      },
      er => console.log(er)
    );
  }
  
  getContingents(): void {
    this.contService.getContingent().subscribe(
      cat => {
        this.contingents = cat;
      },
      er => console.log(er)
    );
  }

  postContingents(): void {
    this.formAdd = this._formBuilder.group({
      contingentId: [""],
      contingentName: [""],
      officialName: [""],
      contingentAddress: [""],
      championshipId: [""]
    });
  }

  deleteContingents(contsId: Contingent) {
    this.contService.deleteContingent(contsId).subscribe(
      cat => {
        this.statusCode = 204;
        alert("Delete Contingent Success");
      },

      er => console.log(er)
    );
    this.getContingents();
  }

  onSubmit(event, contingent: Contingent, isValid: boolean): void {
    event.preventDefault();
    if (isValid) {
      this.id = localStorage.getItem("getChampId")
      contingent.championshipId = this.id;
      this.contService.postContingent(contingent).subscribe(
        res => {
          if (res.status == "success") {
            alert("Register Succes !");
          } else {
            alert(res.message);
          }
        },
        err => console.log(err)
      );
      this.getContingents();
      this._formBuilder.group({
        contingentName: null,
        contingentAddress: null,
        officialName: null
      });
    } else {
      if (
        contingent.contingentName == "" &&
        contingent.officialName == "" &&
        contingent.contingentAddress == ""
      ) {
        // alert('Register Failed!');
        alert("Data must be filled!");
      } else if (contingent.contingentName == "") {
        // alert('Register Failed!');
        alert("Contingent Name must be filled!");
      } else if (contingent.officialName == "") {
        // alert('Register Failed!');
        alert("Official Name must be filled!");
      } else if (contingent.contingentAddress == "") {
        // alert('Register Failed!');
        alert("Contingent Address must be filled!");
      } else if (!/^[a-zA-Z]+$/.test(contingent.contingentName)) {
        alert('Contingent Name must contains Character only "A-Z" !');
      } else if (!/^[a-zA-Z]+$/.test(contingent.officialName)) {
        alert('Official Name must contains Character only "A-Z" !');
      }
    }
  }

  // @ViewChild('popup1') popup1: Popup;
  createItem(contingent: Contingent): FormGroup {
    return this._formBuilder.group({
      contingentName: [contingent.contingentName],
      contingentAddress: [contingent.contingentAddress],
      officialName: [contingent.officialName]
    });
  }

  updateContingents(contId: Contingent) {
    this.contService.getContingentId(contId).subscribe(
      part => {
        this.contingetnsId = part;
        this.formEdit = this.createItem(this.contingetnsId);
      },
      er => console.log(er)
    );
  }

  Update(event, contId, contingent: Contingent, isValid: Boolean) {
    event.preventDefault();
    if (isValid) {
      this.contService.editContingent(contId, contingent).subscribe(
        res => {
          if (res.status == "success") {
            alert("Register Succes !");
          } else {
            alert(res.message);
          }
        },

        err => console.log(err)
      );
      // window.location.reload();

      this.getContingents();
    } else {
      if (
        contingent.contingentName == "" &&
        contingent.officialName == "" &&
        contingent.contingentAddress == ""
      ) {
        // alert('Register Failed!');
        alert("Data must be filled!");
      } else if (contingent.contingentName == "") {
        // alert('Register Failed!');
        alert("Contingent Name must be filled!");
      } else if (contingent.officialName == "") {
        // alert('Register Failed!');
        alert("Official Name must be filled!");
      } else if (contingent.contingentAddress == "") {
        // alert('Register Failed!');
        alert("Contingent Address must be filled!");
      } else if (!/^[a-zA-Z]+$/.test(contingent.contingentName)) {
        alert('Contingent Name must contains Character only "A-Z" !');
      } else if (!/^[a-zA-Z]+$/.test(contingent.officialName)) {
        alert('Official Name must contains Character only "A-Z" !');
      }
    }
  }
}
