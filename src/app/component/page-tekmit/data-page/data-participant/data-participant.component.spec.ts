import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataParticipantComponent } from './data-participant.component';

describe('DataParticipantComponent', () => {
  let component: DataParticipantComponent;
  let fixture: ComponentFixture<DataParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
