import {
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

import { ParticipantService } from "../../../../services/participant.service";
import { ClassService } from "../../../../services/class.service";
import { ContingentService } from "../../../../services/contingent.service";

import { Participant } from "../../../../model/participant";
import { Contingent } from "../../../../model/contingent";
import { Classy } from "../../../../model/classy";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: "app-data-participant",
  templateUrl: "./data-participant.component.html",
  styleUrls: ["./data-participant.component.css"],
  providers: [ParticipantService, ClassService, ContingentService]
})
export class DataParticipantComponent implements OnInit {
  private participants: Participant[];
  private participantsId: Participant;
  private classs: Classy[];
  private classIds: Classy;
  private contingents: Contingent[];
  private ageMin;
  private ageMax;
  private min;
  private max;
  private minW;
  private maxW;

  private errorMessage: string;
  statusCode: number;
  public formEdit: FormGroup;
  public formAdd: FormGroup;

  constructor(
    //private popup: Popup,
    private router: Router,
    private _formBuilder: FormBuilder,
    private partService: ParticipantService,
    private clsService: ClassService,
    private viewCon: ViewContainerRef,
    private contService: ContingentService,
    @Inject(DOCUMENT) private _document
  ) {
    this.viewCon = viewCon;
  }

  ngOnInit() {
    this.formAdd = this._formBuilder.group({
      officialId: "",
      contingentId: "",
      classId: "",
      participantName: "",
      contingentName: "",
      age: "",
      gender: "",
      placeOfBirth: "",
      dateOfBirth: "",
      height: "",
      weight: "",
      address: "",
      minAge: "",
      maxAge: "",
      minWeight: "",
      maxWeight: ""
    });

    this.formEdit = this._formBuilder.group({
      officialId: "",
      contingentId: "",
      classId: "",
      participantName: "",
      age: "",
      gender: "",
      placeOfBirth: "",
      dateOfBirth: "",
      height: "",
      weight: "",
      address: "",
      minAge: "",
      maxAge: "",
      minWeight: "",
      maxWeight: ""
    });

    this.getContingents();
    this.getParticipants();
  }

  addPart(event,addPart: any,age,minAge,maxAge,minWeight,maxWeight,isValid: boolean) {
    event.preventDefault();
    if (isValid) {
      let agePart = this._document.getElementById("age_ag_").value;
      let weightPart = addPart.weight;
      let msg = "Register Participant Failed!, ";
      let statusError: boolean = true;
    
      if(statusError){
        if (agePart < this.min || agePart > this.max  ) {
          msg +=
          addPart.participantName +
            " Age range for this group is " +
            this.min +
            " - " +
            this.max +
            " tahun ";
            statusError = false;
        } else if (addPart.height < 50 || addPart.height > 220  ) {
          msg += addPart.participantName + " Height range is 50 cm - 220 cm";
          statusError = false;
        } else if (weightPart < this.minW || weightPart > this.maxW ) {
          msg +=
          addPart.participantName +
            " berat badan tidak sesuai, " +
            addPart.minWeight +
            "kg - " +
            addPart.maxWeight +
            "kg ";
            statusError = false;
        }
        if(statusError == false){
          alert(msg);

        }
      }

      if(statusError == true){
        addPart.age = this._document.getElementById("age_ag_").value;
        this.partService.addParticipant(addPart).subscribe(
          res => {
            if (res.status == "success") {
              alert("Register Succes !");
            } else {
              alert(res.message);
            }
          },
          err => console.log(err)
        );
        this.getParticipants();
      }

    } else {
      let msg = "Register Participant Failed!, ";
      let statusError: boolean = true;

      let agePart = this._document.getElementById("age_bd_").value;
      let weightPart = addPart.weight;

      if (statusError) {
        if (
          addPart.participantName == "" &&
          addPart.placeOfBirth == "" &&
          addPart.dateOfBirth == "" &&
          addPart.height == "" &&
          addPart.weight == "" &&
          addPart.gender == "" &&
          addPart.address == "" &&
          addPart.classId == "" 
        ) {
          msg += "Form must be filled";
          statusError = false;
        } else if (addPart.participantName == "") {
          msg += "Participant Name is required";
          statusError = false;
        } else if (addPart.gender == "") {
          msg += "Gender is required";
          statusError = false;
        } else if (addPart.placeOfBirth == "") {
          msg += "Place Of Birth is required";
          statusError = false;
        } else if (addPart.dateOfBirth == "") {
          msg += "Date Of Birth is required";
          statusError = false;
        } else if (addPart.height == "") {
          msg += "Height is required";
          statusError = false;
        } else if (addPart.weight == "") {
          msg += "Weight is required";
          statusError = false;
        } else if (addPart.address == "") {
          msg += "Address is required";
          statusError = false;
        } else if (addPart.classId == "") {
          msg += "Class is required";
          statusError = false;
        } else if (agePart < this.min || agePart > this.max) {
          msg +=
            addPart.participantName +
            " Age range for this group is " +
            this.min +
            " - " +
            this.max +
            " tahun ";
          statusError = false;
        } else if (addPart.height < 50 || addPart.height > 220 ) {
          msg += addPart.participantName + " Height range is 50 cm - 220 cm";
          statusError = false;
        } else if (weightPart < this.minW || weightPart > this.maxW ) {
          msg +=
            addPart.participantName +
            " berat badan tidak sesuai, " +
            addPart.minW +
            "kg - " +
            addPart.maxW +
            "kg ";
          statusError = false;
        }
      }

      alert(msg);
    }
  }

  createItem(participant: Participant): FormGroup {
    return this._formBuilder.group({
      officialId: [participant.officialId],
      contingentId: [participant.contingentId],
      classId: [participant.classId],
      participantName: [participant.participantName],
      age: [participant.age],
      gender: [participant.gender],
      placeOfBirth: [participant.placeOfBirth],
      dateOfBirth: [participant.dateOfBirth],
      height: [participant.height],
      weight: [participant.weight],
      address: [participant.address]
    });
  }

  createAdd(participant: Participant): FormGroup {
    return this._formBuilder.group({
      officialId: null,
      contingentId: null,
      classId: null,
      participantName: null,
      age: null,
      gender: null,
      placeOfBirth: null,
      dateOfBirth: null,
      height: null,
      weight: null,
      address: null
    });
  }



  getClasss(gender): void {
    this.clsService.getClassGender(gender).subscribe(
      cat => {
        this.classs = cat;
      },
      er => console.log(er)
    );
  }

  onFilter(cont, kelas, gender, name): void {
    this.partService.getParticipantFilter(name, cont, kelas, gender).subscribe(
      cat => {
        this.participants = cat;
      },
      er => console.log(er)
    );
  }

  selClass(event: any): void {
    let gender = event.target.value;
    this.getClasss(gender);
  }

  selClassId(value) {
    this.clsService.getClassId(value).subscribe(
      cat => {
        this.classIds = cat;
        var today = new Date();
        this.min = this.classIds.minAge;
        this.max = this.classIds.maxAge;
        this.minW = this.classIds.minWeight;
        this.maxW = this.classIds.maxWeight;
        this.ageMin = new Date(today.getFullYear() - this.min, today.getMonth(), today.getDate());
        this.ageMax = new Date(today.getFullYear() - this.max, today.getMonth(), today.getDate());
      },
      er => console.log(er)
    );
   
  }

  updateParticipants(partId: Participant) {
    this.partService.getParticipantId(partId).subscribe(
      part => {
        this.participantsId = part;
        this.getClasss(this.participantsId.gender)
        this.selClassId(this.participantsId.classId)
        this.formEdit = this.createItem(this.participantsId);
        console.log();
      },
      er => console.log(er)
    );
  }

  Update(event, partId: Number, participant: any,isValid: boolean ) {
    event.preventDefault();

    if (isValid) {
      let agePart : number;
      agePart = this._document.getElementById("age_bd_").value;
      if(!agePart){
        agePart = participant.age;
      }
      let msg = "Register Participant Failed!, ";

      let weightPart = participant.weight;
      let statusError : Boolean = true;
      if (statusError) {
        if (agePart < this.min || agePart > this.max  ) {
            msg +=
            participant.participantName +
              " Age range for this group is " +
              this.min +
              " - " +
              this.max +
              " tahun ";
              statusError = false;
          } else if (participant.height < 50 || participant.height > 220  ) {
            msg += participant.participantName + " Height range is 50 cm - 220 cm";
            statusError = false;
          } else if (weightPart < this.minW || weightPart > this.maxW ) {
            msg +=
            participant.participantName +
              " berat badan tidak sesuai, " +
              participant.minW +
              "kg - " +
              participant.maxW +
              "kg ";
              statusError = false;
          } 
          alert(msg);
        }
        
        if(statusError == true){
          participant.age = this._document.getElementById("age_bd_").value;

          this.partService.editParticipant(partId, participant).subscribe(
            res => {
              console.log(res);
              if (res.status == "success") {
                alert("Update Participant Success !");
              } else {
                alert(res.message);
              }
            },
    
            err => console.log(err)
          );
    
          this.getParticipants();
        } else {
          if (statusError) {
            let msg = "Register Participant Failed!, ";
    
              if (
                participant.participantName == "" &&
                participant.placeOfBirth == "" &&
                participant.dateOfBirth == "" &&
                participant.height == "" &&
                participant.weight == "" &&
                participant.gender == "" &&
                participant.address == "" &&
                participant.classId == "" 
              ) {
                msg += "Form must be filled";
                statusError = false;
              } else if (participant.participantName == "") {
                msg += "Participant Name is required";
                statusError = false;
              } else if (participant.gender == "") {
                msg += "Gender is required";
                statusError = false;
              } else if (participant.placeOfBirth == "") {
                msg += "Place Of Birth is required";
                statusError = false;
              } else if (participant.dateOfBirth == "") {
                msg += "Date Of Birth is required";
                statusError = false;
              } else if (participant.height == "") {
                msg += "Height is required";
                statusError = false;
              } else if (participant.weight == "") {
                msg += "Weight is required";
                statusError = false;
              } else if (participant.address == "") {
                msg += "Address is required";
                statusError = false;
              } else if (participant.classId == "") {
                msg += "Class is required";
                statusError = false;
              }  else if (agePart < this.min || agePart > this.max  ) {
                msg +=
                participant.participantName +
                  " Age range for this group is " +
                  this.min +
                  " - " +
                  this.max +
                  " tahun ";
                  statusError = false;
              } else if (participant.height < 50 || participant.height > 220  ) {
                msg += participant.participantName + " Height range is 50 cm - 220 cm";
                statusError = false;
              } else if (weightPart < this.minW || weightPart > this.maxW ) {
                msg +=
                participant.participantName +
                  " berat badan tidak sesuai, " +
                  participant.minWeight +
                  "kg - " +
                  participant.maxWeight +
                  "kg ";
                  statusError = false;
              } 
    
                alert(msg);
    
    
            }
        }
    }
  }

  getParticipants(): void {
    this.partService.getParticipant().subscribe(
      cat => {
        this.participants = cat;
      },
      er => console.log(er)
    );
  }

  deleteParticipants(partId: Participant) {
    this.partService.deleteParticipant(partId).subscribe(
      cat => {
        this.statusCode = 204;
        alert("Delete Participant Success");
      },
      er => console.log(er)
    );
    this.getParticipants();
  }

  getContingents(): void {
    this.contService.getContingent().subscribe(
      cat => {
        this.contingents = cat;
      },
      er => console.log(er)
    );
  }

  onNew() {
    this.formAdd = this.createAdd(this.participantsId);
    this.participantsId = new Participant;
    this.classIds = new Classy;
    this.ageMax = null;
    this.ageMin = null;

  }

  calculateAge(event: any) {
    // console.log(event);
    let index_id = event.target.id;
    let birthdate_value = event.target.value;

    let today = new Date();
    let birthDate = new Date(birthdate_value);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || today.getDate() < birthDate.getDate()) {
      age--;
    }
    if(age > this.min || age < this.max){
      this._document.getElementById("age_" + String(index_id)).value = age;
    } else if(age == undefined){
      alert(" Age range for this group is " + this.min + " - " + this.max + " years ");
    } else {
      alert("Must be select gender and class!");
    }
  }
}
