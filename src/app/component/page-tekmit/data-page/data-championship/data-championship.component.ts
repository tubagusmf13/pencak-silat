import { Component, OnInit,ElementRef, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

import { ChamphionshipService } from "../../../../services/champhionship.service";
import { Router } from "@angular/router";
//import { Popup } from 'ng2-opd-popup';
import { Championship } from "../../../../model/championship";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-data-championship",
  templateUrl: "./data-championship.component.html",
  styleUrls: ["./data-championship.component.css"],
  providers: [ChamphionshipService]
})
export class DataChampionshipComponent implements OnInit {
  private championships: Championship[];
  private championshipsId: Championship;
  private keyChamp = "champhionship";
  public formEdit: FormGroup;
  public formAdd: FormGroup;
  public minDateStart : Date;
  public maxDateStart : Date;
  public minDateEnd: Date;
  public maxDateEnd: Date;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  statusCode: number;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private champService: ChamphionshipService

  ) {}

  ngOnInit() {
    //DATE VALIDATION
    var today = new Date();
    this.minDateStart = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    this.maxDateStart = new Date(today.getFullYear(), today.getMonth() + 6, today.getDate());
    if(this.maxDateStart.getMonth() + 6 > 12){
      this.maxDateStart.getFullYear() + 1;
    }

    this.getChampions();

    this.formEdit = this._formBuilder.group({
      championshipName: "",
      dateStart: "",
      dateEnd: "",
      description: "",
      location: ""
    });

    this.formAdd = this._formBuilder.group({
      championshipName: "",
      dateStart: "",
      dateEnd: "",
      description: "",
      location: ""
    });
  }

  setDateEnd(dataDate : any){
    let datestart = new Date(dataDate);
    this.minDateEnd = new Date(datestart.getFullYear(), datestart.getMonth(), datestart.getDate());
    this.maxDateEnd = new Date(datestart.getFullYear(), datestart.getMonth(), datestart.getDate() + 14);
    console.log(new Date(this.maxDateEnd.getFullYear(), this.maxDateEnd.getMonth(), 0).getDate());
    if(this.maxDateEnd.getDate() > new Date(this.maxDateEnd.getFullYear(), this.maxDateEnd.getMonth(), 0).getDate()){
      this.maxDateEnd.getMonth() + 1;
    }
  }

  getChampions(): void {
    this.champService.getChampionship().subscribe(
      champ => {
        this.championships = champ;
      },
      er => console.log(er)
    );
  }

  deleteChampionships(champId: Championship) {
    this.champService.deleteChampionship(champId).subscribe(
      cat => {
        this.statusCode = 204;
        alert("Delete Championship Success");
      },

      er => console.log(er)
    );
      this.getChampions();
  }

  onSubmit(event, champ: any, isValid: Boolean) {
    event.preventDefault();
    console.log(champ);
    if (isValid) {
      this.champService.postChampionship(champ).subscribe(
        res => {
          console.log(res);
          if (res.status == "success") {
            alert("Register Succes !");
          } else {
            alert(res.message);
          }
        },
        err => console.log(err)
      );
      this.getChampions();

    } else {
      if (
        champ.championshipName == "" &&
        champ.location == "" &&
        champ.description == "" &&
        champ.dateStart == "" &&
        champ.dateEnd == ""
      ) {
        // alert('Register Failed!');
        alert("Data must be filled!");
      } else if (champ.championshipName == "") {
        // alert('Register Failed!');
        alert("Championship Name must be filled!");
      } else if (champ.location == "") {
        // alert('Register Failed!');
        alert("Location must be filled!");
      } else if (champ.description == "") {
        // alert('Register Failed!');
        alert("Description must be filled!");
      } else if (champ.dateStart == "") {
        // alert('Register Failed!');
        alert("Date Start must be filled!");
      } else if (champ.dateEnd == "") {
        // alert('Register Failed!');
        alert("Date End must be filled!");
      } else if (!/^[a-zA-Z]+$/.test(champ.championshipName)) {
        alert('Championship Name must contains Character only "A-Z" !');
      }
    }
  }

  //@ViewChild('popup1') popup1: Popup;
  createItem(champId: Championship): FormGroup {
    return this._formBuilder.group({
      championshipName: [champId.championshipName],
      dateStart: [champId.dateStart],
      dateEnd: [champId.dateEnd],
      description: [champId.description],
      location: [champId.location]
    });
  }

  updateChampionships(champId: Championship) {

    this.champService.getChampionshipId(champId).subscribe(
      part => {
        this.championshipsId = part;
        this.formEdit = this.createItem(this.championshipsId);

      },
      er => console.log(er)
    );
  }

  onFilter(name, start, end): void {
    this.champService.getChampionFilter(name, start, end).subscribe(
      cat => {
        this.championships = cat;
      },
      er => console.log(er)
    );
  }

  Update(event, champId, championshipsId: any, isValid: Boolean) {
    event.preventDefault();
    if (isValid) {
      this.champService.editChampionship(champId, championshipsId).subscribe(
        res => {
          if (res.status == "success") {
            alert("Update Success !");
            this.closeAddExpenseModal.nativeElement.click();

          } else {
            alert(res.message);
          }
        },

        err => console.log(err)
      );
      this.getChampions();
    } else {
      if (
        championshipsId.championshipName == "" &&
        championshipsId.location == "" &&
        championshipsId.description == "" &&
        championshipsId.dateStart == "" &&
        championshipsId.dateEnd == ""
      ) {
        // alert('Register Failed!');
        alert("Data must be filled!");
      } else if (championshipsId.championshipName == "") {
        // alert('Register Failed!');
        alert("Championship Name must be filled!");
      } else if (championshipsId.location == "") {
        // alert('Register Failed!');
        alert("Location must be filled!");
      } else if (championshipsId.description == "") {
        // alert('Register Failed!');
        alert("Description must be filled!");
      } else if (championshipsId.dateStart == "") {
        // alert('Register Failed!');
        alert("Date Start must be filled!");
      } else if (championshipsId.dateEnd == "") {
        // alert('Register Failed!');
        alert("Date End must be filled!");
      } else if (!/^[a-zA-Z]+$/.test(championshipsId.championshipName)) {
        alert('Championship Name must contains Character only "A-Z" !');
      }
    }
  }
}
