import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataChampionshipComponent } from './data-championship.component';

describe('DataChampionshipComponent', () => {
  let component: DataChampionshipComponent;
  let fixture: ComponentFixture<DataChampionshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataChampionshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataChampionshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
