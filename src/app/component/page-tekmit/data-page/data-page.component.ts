import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { ParticipantService } from '../../../services/participant.service';
import { ContingentService } from '../../../services/contingent.service';

import { Participant } from '../../../model/participant';
import { Contingent } from '../../../model/contingent';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-data-page',
  templateUrl: './data-page.component.html',
  styleUrls: ['./data-page.component.css'],
  providers: [ParticipantService,ContingentService]
})
export class DataPageComponent implements OnInit {

  private participants: Participant[];
  private contingents: Contingent[];
  private errorMessage: string;
  public formAdd: FormGroup;
  statusCode: number;
  
  constructor(private _formBuilder : FormBuilder, private partService : ParticipantService , private contService: ContingentService ) { }

  ngOnInit() {
  }

  getParticipants(): void {
  	this.partService.getParticipant().subscribe(cat =>{this.participants=cat;} , er => console.log(er));	
  }

  getContingents(): void {
  	this.contService.getContingent().subscribe(cat =>{this.contingents=cat;} , er => console.log(er));
  }

  postContingents(): void {

    this.formAdd = this._formBuilder.group({
      contingentId: [''],
      contingentName: [''],
      participantAmount: ['']
    });
  }

  deleteContingents(contsId: Contingent) {
    this.contService.deleteContingent(contsId)
      .subscribe(cat => {this.statusCode = 204;
    },
    er => console.log(er));
  }

  onSubmit(contingent: Contingent): void {
    this.contService.postContingent(contingent).subscribe(
      res=>{
        console.log(res); 
      if(res.status == "success"){alert("Register Succes !")}
      else{alert(res.message)} },
       err=>console.log(err));
  }


}
