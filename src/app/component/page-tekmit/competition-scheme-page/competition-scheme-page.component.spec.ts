import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionSchemePageComponent } from './competition-scheme-page.component';

describe('CompetitionSchemePageComponent', () => {
  let component: CompetitionSchemePageComponent;
  let fixture: ComponentFixture<CompetitionSchemePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitionSchemePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionSchemePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
