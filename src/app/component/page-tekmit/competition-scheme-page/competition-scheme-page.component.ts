import { Component, Renderer2, OnInit, Inject } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

import { ParticipantService } from "../../../services/participant.service";
import { ClassService } from "../../../services/class.service";
import { CompetitionService } from "../../../services/competition.service";

import { Participant } from "../../../model/participant";
import { Classy } from "../../../model/classy";
import { Scheme } from "../../../model/scheme";
import { Competition } from "../../../model/competition";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: "app-competition-scheme-page",
  templateUrl: "./competition-scheme-page.component.html",
  styleUrls: ["./competition-scheme-page.component.css"],
  providers: [ParticipantService, CompetitionService, ClassService]
})
export class CompetitionSchemePageComponent implements OnInit {
  private participants: Participant[];
  private copetitions: Competition[];
  public formViewScheme: FormGroup;
  public formGenerate: FormGroup;
  private competitions: Competition[];
  private classs: Classy[];
  private schemeAll: Scheme[];
  private championshipIdString;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private comptService: CompetitionService,
    private partService: ParticipantService,
    private _renderer2: Renderer2,
    private clsService: ClassService,
    @Inject(DOCUMENT) private _document
  ) {}

  ngOnInit() {
    this.declarationForm();
    this.selScheme();

    localStorage.removeItem("jsonBagan");

    let dataJSon = [];

    localStorage.setItem("jsonBagan", JSON.stringify(dataJSon));

    let s = this._renderer2.createElement("script");
   // s.type = `text/javascript`;
    s.src = `assets/js/app-js/bagan.js`;

    this._renderer2.appendChild(this._document.body, s);
    this.refreshBagan = 0;
  }

  selClass(event: any): void {
    let gender = event.target.value;
    this.getClasss(gender);
  }

  getClasss(gender: Number): void {
    this.clsService.getClassGender(gender).subscribe(
      cat => {
        this.classs = cat;
      },
      er => console.log(er)
    );
  }

  declarationForm(): void {
    // console.log("PARTICIPANTS POST SERVER");

    this.formGenerate = this._formBuilder.group({
      classId: [""],
      championshipId: [""],
      schemeName: [""]
    });

    this.formViewScheme = this._formBuilder.group({
      schemeId: [""],
      championshipId: ["1"]
    });
  }

  onSubmit(competition: Competition): void {
    localStorage.removeItem("jsonBagan");
    this.championshipIdString = localStorage.getItem("getChampId");
    competition.championship = this.championshipIdString;
    this.comptService.postComptGenerate(competition).subscribe(
      res => {
        if (res.status == "success") {
          alert("Generete scheme Id = " + res.schemeId);
          let idBagan = res.schemeId;
          this.getScheme(idBagan);
        } else {
          alert(res.msg);
        }
      },
      err => console.log(err)
    );
  }

  refreshBagan: any;
  onViewScheme(schemeData: Scheme): void {
    if (schemeData.schemeId > 0) {
      this.getScheme(schemeData.schemeId);
    }
  }

  getScheme(idBagan) {
    localStorage.removeItem("jsonBagan");
    this.comptService.getSchemeId(idBagan).subscribe(
      cat => {
        localStorage.setItem("jsonBagan", JSON.stringify(cat));
        //G.cracket
        let s = this._renderer2.createElement("script");
        s.type = `text/javascript`;
        s.src = `assets/js/app-js/bagan.js`;

        this._renderer2.appendChild(this._document.body, s);
      },
      er => console.log(er)
    );
  }

    //add @kaha
  selScheme(): void {
    this.getSchemeDataAll();
  }

  getSchemeDataAll() {
    this.comptService.getSchemeAll().subscribe(
      cat => {
        this.schemeAll = cat;
      },
      er => console.log(er)
    );
    // this.refresh_gracket();
  }
}
