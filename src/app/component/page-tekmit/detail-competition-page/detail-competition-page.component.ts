import { Component, OnInit, Inject } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  FormArray
} from "@angular/forms";

import { ScheduleService } from "../../../services/schedule.service";
import { ClassService } from "../../../services/class.service";
import { CompetitionService } from "../../../services/competition.service";
import { JurorService } from "../../../services/juror.service";

import { Schedule } from "../../../model/schedule";
import { Classy } from "../../../model/classy";
import { Competition } from "../../../model/competition";
import { Juror } from "../../../model/juror";
import { Score } from "../../../model/score";
import { Arena } from "../../../model/arena";

@Component({
  selector: "app-detail-competition-page",
  templateUrl: "./detail-competition-page.component.html",
  styleUrls: ["./detail-competition-page.component.css"],
  providers: [ScheduleService, CompetitionService, JurorService, ClassService]
})
export class DetailCompetitionPageComponent implements OnInit {
  curDate: Date;
  maxDate: Date;
  private schedules: Schedule[];
  private schedulesTable: Schedule[];
  private schedulesss: Schedule[];
  private scheduleGet: Schedule[];
  private session: Schedule[];
  private jurors: Schedule[];
  private grands: Schedule[];
  private rounds: Score[];
  private arenas: Arena[];
  private endTime;
  private sched: Schedule;
  public dateSet: Date;
  public today;
  public champStart;
  public champEnd;


  private copetitions: Competition[];
  public formGenerate: FormGroup;
  private competitions: Competition[];
  private classs: Classy[];
  public formSetJadwal: FormGroup;
  public formDelete: FormGroup;
  public items: FormArray;
  public isSumbitted: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private comptService: CompetitionService,
    private schedService: ScheduleService,
    private clsService: ClassService,
    private jurorService: JurorService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.getJurors();
    this.getGrands();
    this.getArenas();
    this.getSchedules();

    this.champStart = localStorage.getItem("getChampDateStart");
    this.champEnd = localStorage.getItem("getChampDateEnd");

    var today = new Date();

    this.today = new Date(today.getFullYear(), today.getMonth(), today.getDate());

    this.formSetJadwal = this.fb.group({
      competitionId: this.fb.array([]),
      classId: [""],
      timeStart: [""],
      timeEnd: [""],
      date: [""],
      arenaId: [""],
      qualification: [""],
      grandJurorId: [""],
      jurorId1: [""],
      jurorId2: [""],
      jurorId3: [""],
      jurorId4: [""],
      jurorId5: [""]
    });

    this.formDelete = this._formBuilder.group({
      date: "",
      arenaId: "",
      timeStart: "",
      timeEnd: ""
    });

    //const items = <FormArray>this.formSetJadwal.controls.competitionId;
  }

  onView(kelas, round): void {
    this.schedService.getScheduleScheme(kelas, round).subscribe(
      cat => {
        this.schedules = cat;
      },
      er => console.log(er)
    );
  }

  onAdd(schedule: any): void {

    let schedulesss: Schedule[];
    for (let i = 0; i < schedule.length; i++) {
      let part = new Schedule();
      part.competitionId = schedule[i].competitionId;
      schedulesss.push(part);
    }
    schedule.timeEnd = this.endTime;
    this.schedService.postSchedules(schedule).subscribe(
      res => {
        if (res.status == "success") {
          alert("Add Success");
          let date = res.date;
          let start = res.timeStart;
          let end = res.timeEnd;
          let arena = res.arenaId;
          this.getSchedule(date, start, end, arena);
          this.getSchedules();
          this.onView(schedule.classId,schedule.qualification  );
        } else {
          alert(res.messasge);
        }
      },
      err => console.log(err)
    );
  }

  getSchedule(date, start, end, arena) {
    this.schedService.getSchedules(date, start, end, arena).subscribe(
      cat => {
        this.scheduleGet = cat;
      },
      er => console.log(er)
    );
  }

  selClass(event: any): void {
    let gender = event.target.value;
    this.getClasss(gender);
  }

  getClasss(gender: Number): void {
    this.clsService.getClassGender(gender).subscribe(
      cat => {
        this.classs = cat;
      },
      er => console.log(er)
    );
  }

  getRounds(classId: Number): void {
    this.comptService.getRoundClass(classId).subscribe(
      cat => {
        this.rounds = cat;
      },
      er => console.log(er)
    );
  }

  setSession(arena, date) {
    this.schedService.getSession(arena, date).subscribe(
      cat => {
        this.session = cat;
      },
      er => console.log(er)
    );
  }

  deleteSchedule(schedDel : Schedule, dated : Date, arena : number, start , end) {
    schedDel.date = dated;
    schedDel.arenaId = arena;
    schedDel.timeStart = start;
    schedDel.timeEnd = end;
    this.schedService.deleteSchedule(schedDel).subscribe(
      cat => {
        this.session = cat;
      },
      er => console.log(er)
    );
    this.getSchedules();
  }

  onFilter(arena, date, time): void {
    this.schedService.getScheduleFilter(arena, date, time).subscribe(
      cat => {
        this.schedulesTable = cat;
      },
      er => console.log(er)
    );
  }

  onFinish() {
    location.reload();
  }

  onChange(email: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.formSetJadwal.controls.competitionId;

    if (isChecked) {
      emailFormArray.push(new FormControl(email));
    } else {
      let index = emailFormArray.controls.findIndex(x => x.value == email);
      emailFormArray.removeAt(index);
    }
  }

  setTimeEnd(start) {
    var hours: number;
    hours = start.split(":")[0];
    var minutes = start.split(":")[1];

    if (hours > 5 && hours < 21) {
      hours = Math.floor(hours * 60);
      hours = hours + 240;
      hours = Math.floor(hours / 60);

      this.endTime = hours + ":" + minutes;
    } else {
      alert("Time Start should be >= 06:00 and <= 20:00.");
    }
  }

  setDate(dateCheck) {
  
    if (new Date(dateCheck) < new Date(this.champStart) || new Date(dateCheck) > new Date(this.champEnd)) {
      alert("Start date should not be before today.");
      dateCheck = this.curDate;
      return null;
    }
  }

  getJurors(): void {
    this.jurorService.getJurors().subscribe(
      champ => {
        this.jurors = champ;
      },
      er => console.log(er)
    );
  }
  getGrands(): void {
    this.jurorService.getGrandJuror().subscribe(
      champ => {
        this.grands = champ;
      },
      er => console.log(er)
    );
  }
  getArenas(): void {
    this.comptService.getArena().subscribe(
      champ => {
        this.arenas = champ;
      },
      er => console.log(er)
    );
  }

  getSchedules(): void {
    this.comptService.getSchedule().subscribe(
      champ => {
        this.schedulesTable = champ;
      },
      er => console.log(er)
    );
  }
}
