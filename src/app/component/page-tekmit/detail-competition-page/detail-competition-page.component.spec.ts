import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCompetitionPageComponent } from './detail-competition-page.component';

describe('DetailCompetitionPageComponent', () => {
  let component: DetailCompetitionPageComponent;
  let fixture: ComponentFixture<DetailCompetitionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCompetitionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCompetitionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
