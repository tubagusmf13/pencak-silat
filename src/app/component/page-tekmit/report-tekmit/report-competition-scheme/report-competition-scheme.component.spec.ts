import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCompetitionSchemeComponent } from './report-competition-scheme.component';

describe('ReportCompetitionSchemeComponent', () => {
  let component: ReportCompetitionSchemeComponent;
  let fixture: ComponentFixture<ReportCompetitionSchemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCompetitionSchemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCompetitionSchemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
