import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportListParticipantComponent } from './report-list-participant.component';

describe('ReportListParticipantComponent', () => {
  let component: ReportListParticipantComponent;
  let fixture: ComponentFixture<ReportListParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportListParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportListParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
