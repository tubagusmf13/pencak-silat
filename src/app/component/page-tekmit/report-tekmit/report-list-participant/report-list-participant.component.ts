import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';

import { Participant } from '../../../../model/participant';
import { Router } from '@angular/router';
import { ParticipantService } from '../../../../services/participant.service';


@Component({
  selector: 'app-report-list-participant',
  templateUrl: './report-list-participant.component.html',
  styleUrls: ['./report-list-participant.component.css'],
  providers: [ParticipantService]
})
export class ReportListParticipantComponent implements OnInit {

  participants: Participant[];

  constructor(private router : Router, private partService : ParticipantService) { }

  printToCart(printSectionId: string){
        let popupWinindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" href="assets/css/material-dashboard.css"><style>@media print {body {-webkit-print-color-adjust: exact;}}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
        popupWinindow.document.close();
        localStorage.removeItem('reportPart');
        this.router.navigate(['/report-tekmit']);


    }

  ngOnInit() {
    
    this.partService.getParticipantClassGender(localStorage.getItem("reportClass"),localStorage.getItem("reportGender"))
    .subscribe(champ =>{ this.participants=champ; }, er => console.log(er));

  }

}