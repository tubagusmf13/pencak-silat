import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ParticipantService } from "../../../services/participant.service";
import { Classy } from "../../../model/classy";
import { ClassService } from "../../../services/class.service";
import { Participant } from "../../../model/participant";
import { CompetitionService } from "../../../services/competition.service";
import { Scheme } from "../../../model/scheme"; // @kaha

@Component({
  selector: "app-report-tekmit",
  templateUrl: "./report-tekmit.component.html",
  styleUrls: ["./report-tekmit.component.css"],
  providers: [ParticipantService, ClassService, CompetitionService]
})
export class ReportTekmitComponent implements OnInit {
  private participants: Participant[];
  private classs: Classy[];
  private schemeAll: Scheme[]; // @kaha


  constructor(
    private routes: Router,
    private partService: ParticipantService,
    private comptService: CompetitionService, // @kaha
    private clsService: ClassService
  ) {}

  ngOnInit() {
    this.selScheme();
  }

  selClass(event: any): void {
    let gender = event.target.value;
    this.getClasss(gender);
  }

  getClasss(gender: Number): void {
    this.clsService.getClassGender(gender).subscribe(
      cat => {
        this.classs = cat;
      },
      er => console.log(er)
    );
  }

  selScheme(): void {
    this.getSchemeDataAll();
  }

  getSchemeDataAll() {
    this.comptService.getSchemeAll().subscribe(
      cat => {
        this.schemeAll = cat;
      },
      er => console.log(er)
    );
  }
  routerScheme(idBagan): void {
    if (idBagan > 0) {
      localStorage.setItem("getidBaganReport", idBagan);
      this.routes.navigate(["/report-compt-scheme"]);
    } else {
      alert("Scheme Name must be chosen!");
    }
  }

  getSelected(kelas, gender): void {
    localStorage.setItem("reportClass", kelas);
    localStorage.setItem("reportGender", gender);

    //service
    this.partService.getParticipantClassGender(
      localStorage.getItem("reportClass"),
      localStorage.getItem("reportGender")
    );

    this.routes.navigate(["/report-list-participant"]);
  }
}
