import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { LoginService } from "../../../services/login.service";

import { User } from "../../../model/user";
import { Router } from "@angular/router";

@Component({
  selector: "app-page-login",
  templateUrl: "./page-login.component.html",
  styleUrls: ["./page-login.component.css"],
  providers: [LoginService]
})
export class PageLoginComponent implements OnInit {
  private newUser: any;
  public myLoginForm: FormGroup;
  public isSumbitted: boolean;
  //public loading:true;
  constructor(
    private _formBuilder: FormBuilder,
    private lService: LoginService,
    private router: Router
  ) {}
  ngOnInit() {
    this.myLoginForm = this._formBuilder.group({
      username: ["", <any>Validators.required],
      password: ["", [<any>Validators.required]]
    });
    console.log(this.myLoginForm);
    this.isSumbitted = false;
  }

  onSubmit(event, user: User, isValid: boolean): void {
    //this.loading = true;
    event.preventDefault();

    this.isSumbitted = true;
    if (isValid) {
      this.lService.SignIn(user).subscribe(
        res => {
          // console.log(res);
          // console.log(user);
          if (res.loginAs == "official") {
            if (res.status == "success") {
              alert("Login Success !");
              localStorage.setItem("official", JSON.stringify(res));
              this.router.navigate(["/championship"]);
            } else if (res.status != "success") {
              alert("Login Failed !");
            }
          } else if (res.loginAs != "official"){
              alert("username / password incorrect")
          }
        },
        error => {
          alert("Server Error");
        }
      );
    }else{
      if(user.username == "" && user.password == ""){
        alert("Username and Password must be filled!");
      }else if (user.username == "") {
        alert("Username must be filled!");
      }else if (user.password == "") {
        alert("Password must be filled!");
      }
    }
    //console.log(JSON.stringify(user), "VALID :");
    //this.isSumbitted=false;
  }
}
