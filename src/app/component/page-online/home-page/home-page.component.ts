import { Component, OnInit } from '@angular/core';
import { ChamphionshipService } from '../../../services/champhionship.service';
import { Router } from '@angular/router';


import { Championship } from '../../../model/championship';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [ChamphionshipService]
})
export class HomePageComponent implements OnInit {

  private championships: Championship[];
  private championship: Championship;

  constructor(private routes : Router, private champService : ChamphionshipService) { }

  ngOnInit() {

    this.getChampions();
  }

  getChampions(): void {
    console.log("CHAMPIONSHIP FROM SERVER");
    this.champService.getChampionship()
    .subscribe(jur =>{this.championships=jur; }, er => console.log(er));
  }

  onDesc(champId): void {
    localStorage.removeItem("champId");
    localStorage.setItem("champId",JSON.stringify(champId));
    this.champService.getChampionshipId(localStorage.getItem("champId"));
    this.routes.navigate(['/event/']);


  }
  

}
