import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Contingent } from '../../../model/contingent';
import { Observable} from 'rxjs/Observable';
import { ContingentService } from '../../../services/contingent.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-register-contingent',
  templateUrl: './register-contingent.component.html',
  styleUrls: ['./register-contingent.component.css'],
  providers: [ContingentService]

})
export class RegisterContingentComponent implements OnInit {

  public formAdd: FormGroup;  
  private keyCont="contingents";
  private contingents: Contingent[];
  public isSumbitted : boolean;
  public dataJson = JSON.parse(localStorage.getItem("official"));

  constructor(private routes : Router, private _formBuilder : FormBuilder, private contService: ContingentService) { }

  ngOnInit() {
    // let 
    // console.log(localStorage);
    // console.log(dataJson);
    this.formAdd = this._formBuilder.group({
      contingentName: ['', <any>Validators.required],
      contingentAddress: ['', <any>Validators.required],
      championshipId: [<number>JSON.parse(localStorage.getItem("idChamp"))],
      officialName: [<string>this.dataJson.officialName],
      officialId: [ <string>this.dataJson.officialId],
    });
    console.log(localStorage);
    this.isSumbitted=false;

  }

  onSubmit(event, contingent: Contingent, isValid: boolean): void {
    event.preventDefault();

    if(isValid) {
      this.contService.postContingent(contingent).subscribe(
        res=>{ 
          if(res.status == "success"){
            alert("Register Success !"); 
            localStorage.setItem("regCont",JSON.stringify(res.contingentId));
            this.routes.navigate(['/participant']);

          }
          else{alert("Register Failed !")} 
        },
        err=>{console.log(err)}
      );
    }else{
      if (contingent.contingentName == "" && contingent.officialName == "" && contingent.contingentAddress == "") {
          // alert('Register Failed!');
        alert("Data must be filled!");
      }else if (contingent.contingentName == "") {
          // alert('Register Failed!');
        alert("Contingent Name must be filled!");
      }else if (contingent.officialName == "") {
          // alert('Register Failed!');
        alert("Official Name must be filled!");
      }else if (contingent.contingentAddress == "") {
          // alert('Register Failed!');
        alert("Contingent Address must be filled!");
      }else if(! /^[a-zA-Z]+$/.test(contingent.contingentName)) {
          alert('Contingent Name must contains Character only "A-Z" !');
      }else if(! /^[a-zA-Z]+$/.test(contingent.officialName)) {
          alert('Official Name must contains Character only "A-Z" !');
      }
    }
  }

}