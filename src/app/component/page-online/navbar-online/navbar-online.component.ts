import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-online',
  templateUrl: './navbar-online.component.html',
  styleUrls: ['./navbar-online.component.css']
})
export class NavbarOnlineComponent implements OnInit {

  constructor(public router: Router) { }
  

  ngOnInit() {
  }
  
  logout() {
    localStorage.removeItem('official');
    this.router.navigate(['/home']);
  }

}
