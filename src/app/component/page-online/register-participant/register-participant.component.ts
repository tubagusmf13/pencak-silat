import { Component, OnInit , Inject} from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, Form, FormArray} from '@angular/forms';
import { Router } from "@angular/router";

import { GroupService } from '../../../services/group.service';
import { ClassService } from '../../../services/class.service';
import { ParticipantService } from '../../../services/participant.service';
import { ContingentService } from '../../../services/contingent.service';

import { Participant } from '../../../model/participant';
import { Group } from '../../../model/group';
import { Classy } from '../../../model/classy';
import { Contingent } from '../../../model/contingent';
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: 'app-register-participant',
  templateUrl: './register-participant.component.html',
  styleUrls: ['./register-participant.component.css'],
  providers: [ParticipantService,GroupService,ClassService,ContingentService]
 
})
export class RegisterParticipantComponent implements OnInit {

  private participants: Participant[];
  private groups: Group[];
  private classs: Classy[];
  public formAdd: FormGroup;
  public items: FormArray;
  public isSumbitted : boolean;
  public birthdate: Array<Date>;
  public ageNumber: [Number];

  constructor(private routes : Router,
              private _formBuilder : FormBuilder, 
              private partService : ParticipantService,
              private grupService: GroupService,
              private contService: ContingentService,
              private clsService: ClassService,
    @Inject(DOCUMENT) private _document) { }

  ngOnInit() {
    this.getGroups();
    //this.getClasss();

    // console.log("PARTICIPANTS POST SERVER");

    this.setTableParticipant();
  }

  calculateAge(event : any) {
    // console.log(event);
    let index_id = event.target.id;
    let birthdate_value = event.target.value;
    
    let today = new Date();
    let birthDate = new Date(birthdate_value);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if(m < 0 || (today.getDate() < birthDate.getDate())) {
      age--;
    }
    // return age;
    // this.ageNumber = this.ageNumber + i;
    // (<HTMLInputElement>document.getElementById("age_"String(index_id))).value;
    this._document.getElementById("age_"+String(index_id)).value = age;
    
    // this.ageNumber[index_id] = age;
    // alert(this.ageNumber);
    // return(this.ageNumber);
    
  
  }

  onSubmit(event, participant: any, isValid: boolean): void {
    event.preventDefault();
// console.log(participant);
    if(isValid) {
      let participants : Participant[]= [];
      for(let i=0; i < participant.items.length; i++ ){

        let part = new Participant();
        part.address = participant.items[i].address;
        part.age = this._document.getElementById("age_bd_"+String(participant.items[i].indexNum)).value;
        part.classId = participant.items[i].classId;
        part.dateOfBirth = participant.items[i].dateOfBirth;
        part.placeOfBirth = participant.items[i].placeOfBirth;
        part.height = participant.items[i].height;
        part.weight = participant.items[i].weight;
        part.gender = participant.items[i].gender;
        part.participantName = participant.items[i].participantName;
        part.contingentId = <number>JSON.parse(localStorage.getItem("regCont"));
        participants.push(part);
        this.isSumbitted=true;

      }

      this.partService.postParticipant(participants).subscribe(
        res=>{
          // console.log(res); 
          if(res.status == "success"){
            alert("Register Participant Success !");
            this.routes.navigate(['/participant']);
            this.setTableParticipant();
          }else{
            alert("Register Failed !")
          }
        },
        err=>console.log(err)
        );
    }else{
      let msg = "Register Participant Failed!, ";
      let statusError: boolean = true;
      for(let i=0; i < participant.items.length; i++ ){
        let agePart = this._document.getElementById("age_bd_"+String(participant.items[i].indexNum)).value;
        let weightPart = participant.items[i].weight;

        if(statusError){
          if (participant.items[i].participantName == "" && participant.items[i].placeOfBirth == "" && participant.items[i].dateOfBirth == "" && participant.items[i].height == "" && participant.items[i].weight == "") {
            msg += "Form must be filled";
            statusError = false;
          }else if (participant.items[i].participantName == "") {
            msg += "Participant Name is required";
            statusError = false;
          }else if (participant.items[i].placeOfBirth == "") {
            msg += "Place Of Birth is required";
            statusError = false;
          }else if (participant.items[i].dateOfBirth == "") {
            msg += "Date Of Birth is required";
            statusError = false;
          }else if (participant.items[i].height == "") {
            msg += "Height is required";
            statusError = false;
          }else if (participant.items[i].weight == "") {
            msg += "Weight is required";
            statusError = false;
          }else if (agePart < participant.items[i].minAge || agePart > participant.items[i].maxAge) {
            msg += participant.items[i].participantName+" Age range for this group is "+participant.items[i].minAge+" - "+participant.items[i].maxAge+" tahun ";
            statusError = false;
          }else if (participant.items[i].height < 50 || participant.items[i].height > 220) {
            msg += participant.items[i].participantName+" Height range is 50 cm - 220 cm";
            statusError = false;
          }else if (weightPart < participant.items[i].minWeight || weightPart > participant.items[i].maxWeight) {
            msg += participant.items[i].participantName+" berat badan tidak sesuai, "+participant.items[i].minWeight+"kg - "+participant.items[i].maxWeight+"kg ";
            statusError = false;
          }
          
        }
      }

      alert(msg);
    }
  }

  createItem2( participant: Participant[]): FormGroup{
    let participants : Participant[]= [];

    return this._formBuilder.group({
      

    })
  }

  createItem(indexNumData : any,valueData :any): FormGroup {
    return this._formBuilder.group({        
        // checkParticipant: [false],
        indexNum: [indexNumData],
        classId: [valueData.classId],
        minAge: [valueData.minAge],
        maxAge: [valueData.maxAge],
        minWeight: [valueData.minWeight],
        maxWeight: [valueData.maxWeight],
        className: [valueData.className],
        participantName: ['', <any>Validators.required],
        groupId: [valueData.groupId],
        gender: [valueData.gender],
        age: [''],
        placeOfBirth: ['', <any>Validators.required],
        dateOfBirth: ['', <any>Validators.required],
        height: ['',<any>Validators.required],
        weight: ['',<any>Validators.required],
        address: ['', <any>Validators.required],
      }
      
    );

  }




  addMore(item:any): void {
    //this.items.push(this.createItem());
  }

  selGroup(event : any):void {
    let grpId = event.target.value;
    //alert("View"+grpId);
    this.getClasss(grpId);
  }

  getGroups(): void {
    // console.log("GROUP FROM SERVER");
    this.grupService.getGroup()
    .subscribe(cat =>{this.groups=cat;
  } , er => console.log(er));	
  }

  setTableParticipant(): void{
    // this.formAdd.reset();
    this.formAdd = this._formBuilder.group({
      groupIdP: [''],
      items: this._formBuilder.array([])
        });

    this.items= <FormArray>this.formAdd.controls['items'];
  }

  getClasss(grpId:Number): void {
    // console.log("CLASS FROM SERVER");
    this.clsService.getClass(grpId).subscribe(
      cat =>{
        // console.log(cat);
        this.classs=cat; 
        this.setTableParticipant();
        this.items.reset();
        let indexNumData = 0;
        this.classs.forEach((value) => {
          this.items.push(this.createItem(indexNumData,value)); indexNumData = indexNumData + 1; });
      } , er => console.log(er)
      );	
  }

}
