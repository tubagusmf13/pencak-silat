import { Component, OnInit } from '@angular/core';
import { ChamphionshipService } from '../../../services/champhionship.service';

import { Championship } from '../../../model/championship';
import { Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-event-championship',
  templateUrl: './event-championship.component.html',
  styleUrls: ['./event-championship.component.css'],
  providers: [ChamphionshipService]

})
export class EventChampionshipComponent implements OnInit {

  private championship: Championship;
  private keyChamp="champhionship";
  

  constructor(private champService : ChamphionshipService) { }

  ngOnInit() {
    
    console.log("CHAMPIONSHIP FROM SERVER");
    this.champService.getChampionshipId(localStorage.getItem("champId"))
    .subscribe(champ =>{ this.championship=champ; }, er => console.log(er));

    
  }

 

}
