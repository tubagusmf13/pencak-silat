import { Component, OnInit } from '@angular/core';


import { Medal } from '../../../../model/medal';
import { Router } from '@angular/router';
import { ChamphionshipService } from '../../../../services/champhionship.service';

@Component({
  selector: 'app-report-medal-list',
  templateUrl: './report-medal-list.component.html',
  styleUrls: ['./report-medal-list.component.css'],
  providers: [ChamphionshipService]
})
export class ReportMedalListComponent implements OnInit {

  medal: Medal[];
  jumlahMedalGold: number;
  jumlahMedalSilver: number;
  jumlahMedalBronze: number;
  public champTgl;
  public champName;
  public champLocation;
  constructor(private router : Router, private medalService : ChamphionshipService) { }

  printToCart(printSectionId: string){
        let popupWinindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" href="assets/css/material-dashboard.css"><style>@media print {body {-webkit-print-color-adjust: exact;}}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
        popupWinindow.document.close();
    }

  ngOnInit() {

    this.champTgl = localStorage.getItem("getChampTgl");
    this.champName = localStorage.getItem("getChampName");
    this.champLocation = localStorage.getItem("getChampLocation");
    
    this.medalService.getMedal(localStorage.getItem("getChampId"))
    .subscribe(champ =>{ 
      this.medal=champ;
      this.sumMedal(this.medal);
    }, er => console.log(er));
  }

  sumMedal(medals:Medal[]){
    this.jumlahMedalGold=0;
    this.jumlahMedalSilver=0;
    this.jumlahMedalBronze=0;
    for (let index = 0; index < medals.length; index++) {
     this.jumlahMedalGold = this.jumlahMedalGold + medals[index].totalGoldMedal;
     this.jumlahMedalSilver = this.jumlahMedalSilver + medals[index].totalSilverMedal;
     this.jumlahMedalBronze = this.jumlahMedalBronze + medals[index].totalBronzeMedal;
    }
  }



}
