import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMedalListComponent } from './report-medal-list.component';

describe('ReportMedalListComponent', () => {
  let component: ReportMedalListComponent;
  let fixture: ComponentFixture<ReportMedalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportMedalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportMedalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
