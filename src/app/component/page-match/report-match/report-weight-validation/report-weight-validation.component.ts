import { Component, OnInit } from '@angular/core';
import { CompetitionService } from "../../../../services/competition.service";
import { Score } from "../../../../model/score";

@Component({
  selector: 'app-report-weight-validation',
  templateUrl: './report-weight-validation.component.html',
  styleUrls: ['./report-weight-validation.component.css'],
  providers: [CompetitionService]
})
export class ReportWeightValidationComponent implements OnInit {

  private scores: Score[];
  public champTgl;
  public champName;
  public champLocation;
  public dateWei;
  public contentData;

  constructor(
    private comptService: CompetitionService, // @kaha
    ) { }

	printToCart(printSectionId: string){
        let popupWinindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" href="assets/css/material-dashboard.css"><style>@media print {body {-webkit-print-color-adjust: exact;}}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
        popupWinindow.document.close();
  	}

  ngOnInit() {
    this.champTgl = localStorage.getItem("getChampTgl");
    this.champName = localStorage.getItem("getChampName");
    this.champLocation = localStorage.getItem("getChampLocation");
    this.dateWei = new Date(localStorage.getItem("getdateReportWei"));
    
    this.comptService.getScore(localStorage.getItem("getcomptIdReportWei")).subscribe(
      cat => {
        this.contentData = cat[0];
      },
      er => console.log(er)
    );
  }
}
