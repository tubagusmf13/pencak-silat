import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportWeightValidationComponent } from './report-weight-validation.component';

describe('ReportWeightValidationComponent', () => {
  let component: ReportWeightValidationComponent;
  let fixture: ComponentFixture<ReportWeightValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportWeightValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportWeightValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
