import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { Classy } from "../../../model/classy";
import { ClassService } from "../../../services/class.service";
import { Competition } from "../../../model/competition";
import { CompetitionService } from "../../../services/competition.service";
import { Scheme } from "../../../model/scheme"; // @kaha
import { Monitoring } from "../../../model/monitoring";// @kaha

@Component({
  selector: 'app-report-match',
  templateUrl: './report-match.component.html',
  styleUrls: ['./report-match.component.css'],
  providers: [CompetitionService, ClassService]
})
export class ReportMatchComponent implements OnInit {
  private competition: Competition[];
  private classs: Classy[];
  private schemeAll: Scheme[]; // @kaha
  private compWei: Monitoring[];// @kaha
  private ScheduleSessionWei: Monitoring[];// @kaha
  private ScheduleSessionDtl: Monitoring[];// @kaha

  constructor(private routes: Router,
    private partService: CompetitionService,
    private comptService: CompetitionService, // @kaha
    private clsService: ClassService) {}

  ngOnInit() {
    this.selScheme(); // @kaha
  }

    selGender(event: any): void {
      let gender = event.target.value;
      this.getClasss(gender);
    }
  
    getClasss(gender: Number): void {
      this.clsService.getClassGender(gender).subscribe(
        cat => {
          this.classs = cat;
        },
        er => console.log(er)
      );
    }

    // begin @kaha
    selScheme(): void {
      this.getSchemeDataAll();
    }

    getSchemeDataAll() {
      this.comptService.getSchemeAll().subscribe(
        cat => {
          this.schemeAll = cat;
        },
        er => console.log(er)
      );
    }
    routerScheme(idBagan): void{
      if(idBagan > 0){
        localStorage.setItem("getidBaganReport", idBagan);
        this.routes.navigate(['/report-compt-scheme']);
      }else{
        alert('Scheme Name must be chosen!');
      }
      
    }

    selArenaDtl(dateGet : any, event : any): void {
      let arena = event.target.value;

      this.comptService.getScheduleSessionList(dateGet, arena)
                       .subscribe(list =>{this.ScheduleSessionDtl=list; }, er => console.log(er)); 
    }
    routerDetail(url, arena) {

      if(url != '' && arena > 0){
        localStorage.setItem("geturlReportDtl", url);
        localStorage.setItem("getarenaReportDtl", arena);
        this.routes.navigate(['/report-compt-schedule']);
      }else{
        alert('Arena and Schedule session must be chosen!');
      }
    }
    selArenaWei(dateGet : any, event : any): void {
      let arena = event.target.value;

      this.comptService.getScheduleSessionList(dateGet, arena)
                       .subscribe(list =>{this.ScheduleSessionWei=list; }, er => console.log(er)); 
    }
    onShows(url: String, arena) {
      this.comptService.getMonitoringByDate(url, arena).subscribe(
        cat => {
          this.compWei = cat;
        },
        er => console.log(er)
      );
    }
    routerWeight(comptId, dateWei) {
      if(comptId != '' && dateWei != 0){
        localStorage.setItem("getdateReportWei", dateWei);
        localStorage.setItem("getcomptIdReportWei", comptId);
        this.routes.navigate(['/report-weight-validation']);
      }else{
        alert('Arena and Schedule session must be chosen!');
      }
    }
}
