import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportComptScheduleComponent } from './report-compt-schedule.component';

describe('ReportComptScheduleComponent', () => {
  let component: ReportComptScheduleComponent;
  let fixture: ComponentFixture<ReportComptScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportComptScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComptScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
