import { Component, OnInit } from '@angular/core';
import { CompetitionService } from "../../../../services/competition.service";
import { Monitoring } from "../../../../model/monitoring";

@Component({
  selector: 'app-report-compt-schedule',
  templateUrl: './report-compt-schedule.component.html',
  styleUrls: ['./report-compt-schedule.component.css'],
  providers: [CompetitionService]
})
export class ReportComptScheduleComponent implements OnInit {

  private monitors: Monitoring[];
  public champTgl;
  public champName;
  public champLocation;
  public arena;
  public round;

  constructor(
    private comptService: CompetitionService, // @kaha
    ) { }

	printToCart(printSectionId: string){
        let popupWinindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" href="assets/css/material-dashboard.css"><style>@media print {body {-webkit-print-color-adjust: exact;}}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
        popupWinindow.document.close();
  	}

  ngOnInit() {
    this.champTgl = localStorage.getItem("getChampTgl");
    this.champName = localStorage.getItem("getChampName");
    this.champLocation = localStorage.getItem("getChampLocation");
    this.arena = localStorage.getItem("getarenaReportDtl");
    let dataUrl = localStorage.getItem("geturlReportDtl").split("&");
    this.round = dataUrl[0].substring(7);
    
    this.comptService.getMonitoringByDate(localStorage.getItem("geturlReportDtl"),localStorage.getItem("getarenaReportDtl")).subscribe(
      cat => {
        this.monitors = cat;
        if(cat.message != null){
          this.monitors=[];
        }
        
      },
    );
  }


}
