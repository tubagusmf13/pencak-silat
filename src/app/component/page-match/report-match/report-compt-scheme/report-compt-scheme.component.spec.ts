import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportComptSchemeComponent } from './report-compt-scheme.component';

describe('ReportComptSchemeComponent', () => {
  let component: ReportComptSchemeComponent;
  let fixture: ComponentFixture<ReportComptSchemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportComptSchemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComptSchemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
