import { Component, Renderer2, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from "@angular/platform-browser";
import { CompetitionService } from "../../../../services/competition.service";

@Component({
  selector: 'app-report-compt-scheme',
  templateUrl: './report-compt-scheme.component.html',
  styleUrls: ['./report-compt-scheme.component.css'],
  providers: [ CompetitionService]
})
export class ReportComptSchemeComponent implements OnInit {

  public champTgl;
  public champName;
  public champLocation;

  constructor(
    private _renderer2: Renderer2,
    private comptService: CompetitionService,
    @Inject(DOCUMENT) private _document
    ) { }

  ngOnInit() {

    this.champTgl = localStorage.getItem("getChampTgl");
    this.champName = localStorage.getItem("getChampName");
    this.champLocation = localStorage.getItem("getChampLocation");
    
    localStorage.removeItem("jsonBagan");
    this.comptService.getSchemeId(localStorage.getItem("getidBaganReport")).subscribe(
      cat => {
        localStorage.setItem("jsonBagan", JSON.stringify(cat));
        //G.cracket
        let s = this._renderer2.createElement("script");
        s.type = `text/javascript`;
        s.src = `assets/js/app-js/bagan.js`;

        this._renderer2.appendChild(this._document.body, s);
      },
      er => console.log(er)
    );
  }

  // printToCart(my_gracket: string){
  //   let popupWinindow
  //   let innerContents = document.getElementById(my_gracket).innerHTML;
  //   popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
  //   popupWinindow.document.open();
  //   popupWinindow.document.write('<html>\
  //       <head>\
  //         <link rel="stylesheet" href="assets/css/material-dashboard.css">\
  //         <style type="text/css">\
  //           .g_gracket { width: 9999px; background-color: #fff; padding: 55px 15px 5px; line-height: 100%; position: relative; overflow: hidden;}\
  //           .g_round { float: left; margin-right: 70px; color: #979596}\
  //           .g_game { position: relative; margin-bottom: 15px; }\
  //           .g_gracket h3 { margin: 0; padding: 10px 8px 8px; font-size: 13px; font-weight: normal; color: #fff}\
  //           .g_team { background: red;}\
  //           .g_team:last-child {  background: blue; }\
  //           .g_round:last-child { margin-right: 20px; }\
  //           .g_winner { background: #444; }\
  //           .g_winner .g_team { background: none; }\
  //           .g_current { cursor: pointer; background: #00ff00!important; color: black !important }\
  //           .g_round_label { top: -5px; font-weight: normal; color: #CCC; text-align: center; font-size: 18px; }\
  //           @media print {\
  //             body {\
  //               -webkit-print-color-adjust: exact;\
  //             }\
  //           }\
  //         </style>\
  //       </head>\
  //       \
  //       <body onload="window.print()" style="background-color: rgb(56, 61, 68)">\
  //         ' + innerContents + '\
  //       </body>\
  //     </html>');
  //   popupWinindow.document.close();
  // }
}
