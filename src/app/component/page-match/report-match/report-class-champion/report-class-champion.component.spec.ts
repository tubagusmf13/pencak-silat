import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportClassChampionComponent } from './report-class-champion.component';

describe('ReportClassChampionComponent', () => {
  let component: ReportClassChampionComponent;
  let fixture: ComponentFixture<ReportClassChampionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportClassChampionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportClassChampionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
