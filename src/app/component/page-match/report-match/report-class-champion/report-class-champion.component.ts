import { Component, OnInit } from '@angular/core';

import { Champion } from '../../../../model/champion';
import { Router } from '@angular/router';
import { ChamphionshipService } from '../../../../services/champhionship.service'
@Component({
  selector: 'app-report-class-champion',
  templateUrl: './report-class-champion.component.html',
  styleUrls: ['./report-class-champion.component.css'],
  providers: [ChamphionshipService]
})
export class ReportClassChampionComponent implements OnInit {

  champion: Champion[];
  public champTgl;
  public champName;
  public champLocation;

  constructor(private router : Router, private championService : ChamphionshipService) { }

  printToCart(printSectionId: string){
        let popupwindow
        let innerContents = document.getElementById(printSectionId).innerHTML;
        popupwindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,date=no,status=no,titlebar=no');
        popupwindow.document.open();
        popupwindow.document.write('<html><head><link rel="stylesheet" href="assets/css/material-dashboard.css"><style>@media print {body {-webkit-print-color-adjust: exact;}}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
        popupwindow.document.close();
        localStorage.removeItem('reportClass');
        this.router.navigate(['/report-match']);
    }

    ngOnInit() {
      this.champTgl = localStorage.getItem("getChampTgl");
      this.champName = localStorage.getItem("getChampName");
      this.champLocation = localStorage.getItem("getChampLocation");

      this.championService.getChampion(localStorage.getItem('getChampId'))
      .subscribe(camp=>{
        this.champion=camp;
      })
    }
}
