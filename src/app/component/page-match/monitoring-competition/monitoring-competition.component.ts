import { Component, OnInit, Inject } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  FormArray
} from "@angular/forms";

import { CompetitionService } from "../../../services/competition.service";

import { Monitoring } from "../../../model/monitoring";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: 'app-monitoring-competition',
  templateUrl: './monitoring-competition.component.html',
  styleUrls: ['./monitoring-competition.component.css'],
  providers: [ CompetitionService]
})
export class MonitoringCompetitionComponent implements OnInit {

  private monitors: Monitoring[];
  private monitorList: Monitoring[];

  private monitor: Monitoring;
  public formScoring: FormGroup;
  public round : any;
  public idcmpt;


  constructor(
    private _formBuilder : FormBuilder, 
    private comptService: CompetitionService,
    @Inject(DOCUMENT) private _document){ }

  ngOnInit() {
    this.formScoring = this._formBuilder.group({
      competitionId: '',
      status:'',
      round: '',
      winner: '',
      winStatus: '',
    });

  }

  selArena(event : any): void {
    let arena = event.target.value;

    this.comptService.getMonitoringList(arena)
                     .subscribe(list =>{this.monitorList=list; }, er => console.log(er));	
  }

  onShows(url : String, arena) {
    this.comptService.getMonitoring(url,arena).subscribe(
      cat => {
        this.monitors = cat;
        if(cat.message != null){
          this.monitors=[];
        }
        
      },
    );

  }

  onStart(monitor : Monitoring, comptId): void{
    monitor.status = 'start';
    monitor.competitionId = comptId;
    alert('Start Round = ' + monitor.round );
    this.comptService.postScoring(monitor).subscribe(
      res=>{
      if(res.status == "success"){
        alert("Start Round Success !");
        this._document.getElementById("stop_"+String(comptId)).disabled= false;
        this._document.getElementById("finish_"+String(comptId)).disabled= true;
        this._document.getElementById("start_"+String(comptId)).disabled= true;
      }
      else{alert(res.message)} },
       err=>console.log(err));
  }

  onStop(monitor : Monitoring, comptId): void{
    monitor.status = 'stop';
    monitor.competitionId = comptId;
    alert('Stop Round = ' + monitor.round );

    console.log(JSON.stringify(monitor));
    this.comptService.postScoring(monitor).subscribe(
      res=>{
      if(res.status == "success"){
        alert("Stop Round Success !");
        this._document.getElementById("round"+monitor.round+"_"+String(comptId)).disabled= true;  
        
        if(monitor.round == 1){
          this._document.getElementById("round2_"+String(comptId)).disabled= false;
          this._document.getElementById("round1_"+String(comptId)).checked = false;
        }else if(monitor.round == 2){
          this._document.getElementById("round3_"+String(comptId)).disabled= false;
          this._document.getElementById("round2_"+String(comptId)).checked = false;
        }
        
        this._document.getElementById("stop_"+String(comptId)).disabled= true;
        this._document.getElementById("start_"+String(comptId)).disabled= false;
        this._document.getElementById("finish_"+String(comptId)).disabled= false;
      }
      else{alert(res.message)} },
       err=>console.log(err));
  }

  sendId(id){
    this.idcmpt = id;
  }

  onFinish(monitor : Monitoring, comptId): void{
    monitor.status = 'finish';
    monitor.competitionId = this.idcmpt;


    if(monitor.round == 2){
      monitor.round = 1;
    }else if(monitor.round == 3){
      monitor.round = 2;
    }
    alert('Finish Round');

    this.comptService.postScoring(monitor).subscribe(
      res=>{
        console.log(res); 
      if(res.status == "success"){alert("Finish Round Success !")}
      else{alert(res.message)} },
       err=>console.log(err));
  }

  

}
