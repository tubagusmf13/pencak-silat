import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringCompetitionComponent } from './monitoring-competition.component';

describe('MonitoringCompetitionComponent', () => {
  let component: MonitoringCompetitionComponent;
  let fixture: ComponentFixture<MonitoringCompetitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringCompetitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringCompetitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
