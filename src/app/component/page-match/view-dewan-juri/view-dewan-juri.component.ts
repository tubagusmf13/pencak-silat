import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { CompetitionService } from "../../../services/competition.service";
import { Monitoring } from "../../../model/monitoring";
import { Score } from "../../../model/score";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-view-dewan-juri",
  templateUrl: "./view-dewan-juri.component.html",
  styleUrls: ["./view-dewan-juri.component.css"],
  providers: [CompetitionService]
})
export class ViewDewanJuriComponent implements OnInit {
  private scores: Score[];
  private monitors: Monitoring[];
  private monitorList: Monitoring[];
  private subscription: Subscription = new Subscription();
  interval: any;

  constructor(
    private comptService: CompetitionService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
  }

  selArena(event: any): void {
    let arena = event.target.value;

    this.comptService.getMonitoringList(arena).subscribe(
      list => {
        this.monitorList = list;
      },
      er => console.log(er)
    );
  }

  onShows(url: String, arena) {
    this.comptService.getMonitoring(url, arena).subscribe(
      cat => {
        this.monitors = cat;
      },
      er => console.log(er)
    );
  }

  getScores(numCompt: number) {
    this.interval = setInterval(() => {
      this.comptService.getScore(numCompt).subscribe(
        cat => {
          this.scores = cat;
        },
        er => console.log(er)
      );
    }, 5000);
  }
}
