import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDewanJuriComponent } from './view-dewan-juri.component';

describe('ViewDewanJuriComponent', () => {
  let component: ViewDewanJuriComponent;
  let fixture: ComponentFixture<ViewDewanJuriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDewanJuriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDewanJuriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
