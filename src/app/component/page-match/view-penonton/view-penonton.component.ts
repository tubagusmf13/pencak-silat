import { Component, OnInit } from "@angular/core";
import { CompetitionService } from "../../../services/competition.service";
import { Monitoring } from "../../../model/monitoring";
import { Score } from "../../../model/score";

@Component({
  selector: "app-view-penonton",
  templateUrl: "./view-penonton.component.html",
  styleUrls: ["./view-penonton.component.css"],
  providers: [CompetitionService]
})
export class ViewPenontonComponent implements OnInit {
  private scores: Score[];
  private monitors: Monitoring[];
  private monitorList: Monitoring[];
  interval: any;

  constructor(private comptService: CompetitionService) {}

  ngOnInit() {}

  selArena(event: any): void {
    let arena = event.target.value;

    this.comptService.getMonitoringList(arena).subscribe(
      list => {
        this.monitorList = list;
      },
      er => console.log(er)
    );
  }

  onShows(url: String, arena) {
    this.comptService.getMonitoring(url, arena).subscribe(
      cat => {
        this.monitors = cat;
      },
      er => console.log(er)
    );
  }

  getScores(numCompt: number) {
    this.interval = setInterval(() => {
      this.comptService.getScore(numCompt).subscribe(
        cat => {
          this.scores = cat;
        },
        er => console.log(er)
      );
    }, 5000);
  }
}
