export class Juror 
{
    jurorId: number;
    jurorName: string;
    championshipId : number;
    role: string;
    gender: string;
    grandJurorId: number;
    grandJurorName: String;
    jurorId1 : number;
    jurorId2 : number;
    jurorId3 : number;
    jurorId4 : number;
    jurorId5 : number;
     
}