export class Competition {

    classId : number ;
    gender : String;
    championship : number;
    championshipId : number ;
    schemeName : String;
    name: String;
    id: number;
    seed: number;

}