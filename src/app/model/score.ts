export class Score {

    round: number;
    roundName: String;
    championshipName: String;
    championshipDateStart: Date;
    championshipDateEnd: Date;
    championshipLocation: String;

    competitionId: number;
    redCornerName: String;
    redCornerContingent: String
    blueCornerName: String;
    blueCornerContingent: String;
    winner: Array< {
        winnerName: String;
        winnerSide: String;
    }>;

    score: Array<{
        redJuror1: String;
        totalRedJuror1: number;
        redJuror2: String;
        totalRedJuror2: number;
        redJuror3: String;
        totalRedJuror3: number;
        redJuror4: String;
        totalRedJuror4: number;
        redJuror5: String;
        totalRedJuror5: number;
        totalScoreRed: number;
        blueJuror1: String;
        totalBlueJuror1: number;
        blueJuror2: String;
        totalBlueJuror2: number;
        blueJuror3: String;
        totalBlueJuror3: number;
        blueJuror4: String;
        totalBlueJuror4: number;
        blueJuror5: String;
        totalBlueJuror5: number;
        totalScoreBlue: number;
      }>;
    
      totalAllRoundRedJuror1: number;
      totalAllRoundRedJuror2: number;
      totalAllRoundRedJuror3: number;
      totalAllRoundRedJuror4: number;
      totalAllRoundRedJuror5: number;
      totalAllRoundBlueJuror1: number;
      totalAllRoundBlueJuror2: number;
      totalAllRoundBlueJuror3: number;
      totalAllRoundBlueJuror4: number;
      totalAllRoundBlueJuror5: number;
  
  }
  