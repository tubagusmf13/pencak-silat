export class Championship
{
    championshipId: number;
    championshipName: string;
    dateStart: Date;
    dateEnd: Date;
    championshipLogo: string;
    description : string;
    location: String;
}