export class Monitoring {
  
  className: String;
  winner: String;
  competitionId: number;
  redCorner: Array<{ participantName: String; contingentName: String }>;
  blueCorner: Array<{ participantName: String; contingentName: String }>;
  status: String;
  round: number;
  timeStart: String;
  timeEnd: String;
  winStatus: String;


}
