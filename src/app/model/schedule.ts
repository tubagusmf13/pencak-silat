export class Schedule {

    competitionId: number;
    timeStart: String;
    timeEnd: String;
    date: Date;
    arenaId: number;
    qualification: number;
    checked : boolean;
    grandJurorId: number;
    grandJurorName: String;
    jurorId1 : number;
    jurorId2 : number;
    jurorId3 : number;
    jurorId4 : number;
    jurorId5 : number;
     

}
