export class Contingent {

    contingentId: number;
    contingentName: string;
    contingentAddress: string;
    participantAmount: number;
    officialId: number;
    officialName: string;
    championshipId: number;

}