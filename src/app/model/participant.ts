export class Participant {
    participantId: number;
    officialId:number;
    contingentId: number;
    classId: number;
    participantName: string;
    age: number;
    gender: string;
    placeOfBirth: string;
    dateOfBirth: string;
    height: string;
    weight: string;
    address: number;
    contingentName: string;
    participantAmount: number;
    championshipId: number;
    groupId: number;
    className: string;
    Gender: string;
}