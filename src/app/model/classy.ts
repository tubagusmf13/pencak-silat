export class Classy {
  classId: number;
  groupId: number;
  className: string;
  gender: string;
  maxAge: number;
  maxWeight: number;
  minAge: number;
  minWeight: number;
}
