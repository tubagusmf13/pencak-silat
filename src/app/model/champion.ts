export class Champion
{
    classId: Number;
    className: String;
    gender: String;
    championNo1: Array<{
        participantName: String;
        contingentName: String
    }>;
    championNo2: Array<{
        participantName: String;
        contingentName: String
    }>;
    championNo3: Array<{
        participantName: String;
        contingentName: String
    }>;

}